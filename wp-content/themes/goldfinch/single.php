<?php

/**
 * The template for displaying all single posts.
 *
 * @package goldfinch
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into goldfinch_single_before
             *
             */
            do_action( 'goldfinch_single_before' );

            get_template_part( 'template-parts/content', 'single' );

            /**
             * Functions hooked into goldfinch_single_after
             *
             * @see 10 goldfinch_post_nav
             */
            do_action( 'goldfinch_single_after' );

        endwhile; ?>

    </section>

<?php
get_footer();