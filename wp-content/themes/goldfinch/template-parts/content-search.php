<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package bias
 * @since  1.0.0
 */

?>

<article <?php post_class(); ?>>

    <?php do_action( 'goldfinch_search' ); ?>

</article>