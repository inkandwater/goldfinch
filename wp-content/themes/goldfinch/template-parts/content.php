<?php

/**
 * Template used to display post content.
 *
 * @package goldfinch
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked in to goldfinch_loop_post
     * 
     * @see 10 goldfinch_post_header
     * @see 20 goldfinch_post_thumbnail
     * @see 30 goldfinch_post_content
     * @see 40 goldfinch_post_meta
     */
    do_action( 'goldfinch_loop_post' );
    ?>

</article>
<!-- /post-<?php the_ID(); ?> -->
