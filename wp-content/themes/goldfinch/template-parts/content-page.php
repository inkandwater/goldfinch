<?php

/**
 * Template used for displaying page content in page.php
 *
 * @package goldfinch
 * @since  1.0.0
 */

?>

<!-- page-<?php the_ID(); ?> -->
<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into goldfinch_page_top
     *
     */
    do_action( 'goldfinch_page_top' );

    /**
     * Functions hooked into goldfinch_page
     * 
     * @see 10 goldfinch_page_header
     * @see 20 goldfinch_page_content
     */
    do_action( 'goldfinch_page' );

    /**
     * Functions hooked in to goldfinch_page_bottom
     *
     */
    do_action( 'goldfinch_page_bottom' );
    ?>

</div>
<!-- /page-<?php the_ID(); ?> -->
