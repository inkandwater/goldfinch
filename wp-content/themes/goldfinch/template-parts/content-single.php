<?php

/**
 * Template used for displaying post content on single pages.
 *
 * @package goldfinch
 * @since  1.0.0
 */

?>

<!-- post-<?php the_ID(); ?> -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    /**
     * Functions hooked into goldfinch_single_post_top
     *
     */
    do_action( 'goldfinch_single_post_top' );

    /**
     * Functions hooked into goldfinch_single_post
     * 
     * @see 10 goldfinch_post_header
     * @see 20 goldfinch_post_thumbnail 
     * @see 30 goldfinch_post_content
     * @see 40 goldfinch_post_meta
     */
    do_action( 'goldfinch_single_post' );

    /**
     * Functions hooked in to goldfinch_single_post_bottom
     *
     */
    do_action( 'goldfinch_single_post_bottom' );
    ?>

</div>
<!-- /post-<?php the_ID(); ?> -->
