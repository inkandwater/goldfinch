<?php

/**
 * The header for our theme.
 *
 * @package goldfinch
 * @since   1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- head -->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">

    <?php wp_head(); ?>

</head>
<!-- /head -->

<!-- body -->
<body <?php body_class(); ?>>

    <?php
    /**
     * Functions hooked into goldfinch_site_before
     *
     * @see 10 goldfinch_off_canvas
     */
    do_action( 'goldfinch_site_before' ); ?>

    <div id="page" class="hfeed site">
        <?php
        /**
         * Functions hooked into goldfinch_header_before
         *
         */
        do_action( 'goldfinch_header_before' ); ?>

        <!-- header -->
        <header id="header" <?php goldfinch\header_class(); ?>>
            <?php
            /**
             * Functions hooked into goldfinch_header
             *
             * @see 10 goldfinch_menu_toggle
             * @see 20 goldfinch_site_branding
             * @see 30 goldfinch_main_navigation
             */
            do_action( 'goldfinch_header' ); ?>
        </header>
        <!-- /header -->

        <?php
        /**
         * Functions hooked into goldfinch_content_before
         *
         */
        do_action( 'goldfinch_content_before' ); ?>

            <?php
            /**
             * Functions hooked into goldfinch_content_top
             *
             */
            do_action( 'goldfinch_content_top' );?>