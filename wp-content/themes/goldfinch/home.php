<?php
/**
 * The main template file.
 *
 * @package bias
 * @since   1.0.0
 */
get_header();

$page_id = get_option( "page_for_posts" );
$thumbnail = get_the_post_thumbnail_url( $page_id, 'full' );
$has_thumbnail = ( has_post_thumbnail($page_id) ) ? 'has-page-thumbnail' : null;
$background_style = ( has_post_thumbnail($page_id) ) ? sprintf( 'style="background-image: url(%s);"', esc_url( $thumbnail ) ) : null;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$total_post_count = wp_count_posts();
$published_post_count = $total_post_count->publish;
$total_pages = ceil( $published_post_count / $posts_per_page ); ?>

    <!-- content-area -->
    <section class="content-area">

        <!-- page-header -->
        <header class="page__header <?php echo $has_thumbnail; ?>" <?php echo $background_style; ?>>

            <div class="page__header-inner">
                <?php
                the_archive_title( '<h1 class="page__title heading--xxl">', '</h1>' );
                the_archive_description( '<p class="page__description mt-0">', '</p>' );
                goldfinch\category_list();

                ( is_paged() ) ? printf( "<div class='paged-count text--small'>%s<span>%s of %s</span></div>",
                    __( "Page ", "bias" ),
                    esc_attr( $paged ),
                    esc_attr( $total_pages ),
                ) : null; ?>
            </div>

        </header>
        <!-- /page-header -->

        <?php if ( have_posts() ) :

            get_template_part( 'loop' );

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;

        do_action( 'bias_content_after_loop' ); ?>

    </section>
    <!-- /content-area -->

<?php
get_footer();