<?php

/**
 * The sidebar containing the main widget area.
 *
 * @package goldfinch
 * @since   1.0.0
 */

if ( ! is_active_sidebar( 'blog-sidebar' ) ) {
    return;
}
?>

<!-- sidebar -->
<aside class="sidebar" role="complementary">
    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
</aside>
<!-- /sidebar -->