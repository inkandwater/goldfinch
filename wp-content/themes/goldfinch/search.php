<?php
/**
 * The template for displaying search results pages.
 *
 * @package goldfinch
 * @since   1.0.0
 */
get_header();

global $wp_query;
$total_results = $wp_query->found_posts; ?>

    <!-- content-area -->
    <section class="content-area">

        <?php do_action( 'goldfinch_search_page_before' ); ?>

        <!-- page-header -->
        <header class="page__header">
            <div class="page__header-inner">
                <h1 class="page__title entry-title heading--xxl">
                    <?php echo __( "Search Results", 'goldfinch' ); ?>
                </h1>
                <?php printf( __( '<p>Found %1s Results for: <span class="search-term">%2s</span>', 'goldfinch' ), esc_html( $total_results ), get_search_query() ); ?>
            </div>
        </header>
        <!-- /page-header -->

        <!-- search-results -->
        <div class="search-results">

            <?php do_action( "goldfinch_search_before" ); ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'template-parts/content', 'search' ); ?>

            <?php endwhile; ?>

            <?php do_action( "goldfinch_search_after" ); ?>

        </div><!-- /search-results -->

        </section>

<?php get_footer();