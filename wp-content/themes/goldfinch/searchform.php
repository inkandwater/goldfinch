<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label for="search"><span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span></label>
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"> <g transform="translate(-2 -2)" fill="none"> <circle cx="8" cy="8" r="8" transform="translate(3 3)"/> <path d="m21 21-4.35-4.35"/> </g> </svg>
    <input id="search" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
</form>