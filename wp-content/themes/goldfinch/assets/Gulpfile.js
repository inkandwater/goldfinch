'use strict';

/* --------------------------
# Dependencies
---------------------------*/


const autoprefixer      = require( "gulp-autoprefixer" );
const notify            = require( "gulp-notify" );
const sourcemaps        = require( "gulp-sourcemaps" );
const merge             = require( "merge-stream" );
const sass              = require('gulp-sass')(require('node-sass'));
const gulp              = require( "gulp" );
const { watch, series } = require( "gulp" );
const imagemin          = require( "gulp-imagemin" );

const svgmin      = require( "gulp-svgmin" );
const svgSprite   = require( "gulp-svg-sprite" );
const plumber     = require( "gulp-plumber" );


/* --------------------------
# Configuration
---------------------------*/

let config = {
    imgPath: './src/img',
    sassPath: './src/sass',
    npmDir: './node_modules',
    cssBuildPath: './build/css',
    imgBuildPath: './build/img'
}

/* --------------------------
# Sass Compile
---------------------------*/

gulp.task('styles', () => {

    let main = gulp.src( config.sassPath + '/style.scss' )
      .pipe( sourcemaps.init() )
      .pipe( sass({
          outputStyle: 'expanded',
          includePaths: [
            config.sassPath,
            config.npmDir + '/normalize.css',
            config.npmDir + '/bourbon-neat/core',
            config.npmDir + '/modularscale-sass/stylesheets'
          ]
      })
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      })))
      .pipe( autoprefixer() )
      .pipe( sourcemaps.write('./maps') )
      .pipe( gulp.dest('../') );

    let admin = gulp.src( config.sassPath + '/admin.scss' )
      .pipe( sourcemaps.init() )
      .pipe( sass({
          outputStyle: 'compressed',
          includePaths: [
            config.sassPath,
            config.npmDir + '/bourbon-neat/core',
            config.npmDir + '/modularscale-sass/stylesheets'
          ]
      })
      .on("error", notify.onError(function (error) {
        return "Error: " + error.message;
      })))
      .pipe( autoprefixer() )
      .pipe( sourcemaps.write('./maps') )
      .pipe( gulp.dest( config.cssBuildPath ) );

    return merge( main, admin );

});

/* --------------------------
# Images
---------------------------*/

gulp.task( 'images', () => {

 return gulp.src( config.imgPath + '/*' )
    .pipe( svgmin({
      js2svg: {
        pretty: true
      },
      plugins: [
        {
          cleanupNumericValues: {
            floatPrecision: 2
          }
        },
        {
          removeAttrs: {
            attrs: 'data.*'
          }
        },
        {
          removeViewBox: false
        },
        {
          cleanupIDs: false
        },
        "convertTransform"
      ]
    }
    ))
    .pipe( gulp.dest( './build/img' ) )

});

gulp.task( 'sprite', () => {

    // Basic configuration example
    const svgConfig = {
      mode: {
        symbol: {
          sprite: "../sprite.svg"
        }
      },
      svg: { // General options for created SVG files
        xmlDeclaration: false, // Add XML declaration to SVG sprite
        namespaceClassnames: false
      }
    };

  return gulp.src( '*.svg', { cwd: config.imgBuildPath } )
  .pipe( plumber() )
  .pipe( svgSprite(svgConfig) )
  .on( 'error', function( error ) {
    console.log( error );
  })
  .pipe( gulp.dest( './build/img/svg/' ) );

});

/* --------------------------
# Watch Tasks
---------------------------*/

gulp.task('watch', () => {

    // Watch the input folder for change,
    // and run `sass` task when something happens
    return gulp.watch( config.sassPath + '/**/*.scss', series( ['styles'] ) )

    .on('change', function( path, stats ) {
      console.log(`File ${path} was changed`);
    })

    .on('add', function(path, stats) {
      console.log(`File ${path} was added`);
    })

    .on('unlink', function(path, stats) {
      console.log(`File ${path} was removed`);
    });


});

/* --------------------------
# Dependencies
---------------------------*/

gulp.task( 'build', gulp.series([ 'styles' ]) );
gulp.task( 'default', gulp.series([ 'styles','watch' ]) );