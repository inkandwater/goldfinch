!function(t){var e={};function n(o){if(e[o])return e[o].exports;var i=e[o]={i:o,l:!1,exports:{}};return t[o].call(i.exports,i,i.exports,n),i.l=!0,i.exports}n.m=t,n.c=e,n.d=function(t,e,o){n.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:o})},n.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},n.t=function(t,e){if(1&e&&(t=n(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var o=Object.create(null);if(n.r(o),Object.defineProperty(o,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var i in t)n.d(o,i,function(e){return t[e]}.bind(null,i));return o},n.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return n.d(e,"a",e),e},n.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},n.p="",n(n.s=3)}([function(t,e){t.exports=function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")},t.exports.__esModule=!0,t.exports.default=t.exports},function(t,e){function n(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}t.exports=function(t,e,o){return e&&n(t.prototype,e),o&&n(t,o),Object.defineProperty(t,"prototype",{writable:!1}),t},t.exports.__esModule=!0,t.exports.default=t.exports},function(t,e){!function(t){var e=t("#main-navigation");function n(){t(document.body).on("touchstart",(function(e){t(e.target).closest(".main-navigation li").length||t(".main-navigation li").removeClass("focus")})),e.find(".navigation__nav-item--has-children > a").on("touchstart",(function(e){var n=t(this).parent("li");n.hasClass("focus")||(e.preventDefault(),n.toggleClass("focus"),n.siblings(".focus").removeClass("focus"))}))}e.length&&e.children().length&&(("ontouchstart"in window||navigator.maxTouchPoints)&&(t(window).on("resize",n),n()),e.find("a").on("focus blur",(function(){t(this).parents(".navigation__nav-item").toggleClass("focus")})))}(jQuery)},function(t,e,n){"use strict";n.r(e);var o,i=n(0),s=n.n(i),a=n(1),r=n.n(a),c=function(){function t(){s()(this,t),this.settings={toggle:document.getElementById("navigation-toggle"),body:document.getElementsByTagName("body")[0],activeClass:"js-nav-open",bodyActiveClass:"body--inactive"}}return r()(t,[{key:"init",value:function(){null!==this.settings.toggle&&this.bindUIActions()}},{key:"bindUIActions",value:function(){var t=this,e=this.settings;window.addEventListener("resize",(function(){window.innerWidth>899&&t.activeClass("remove")})),e.toggle.addEventListener("click",(function(){t.activeClass()}))}},{key:"activeClass",value:function(t){var e=this.settings;e.body.classList.contains(e.activeClass)||"remove"===t?(e.body.classList.remove(e.activeClass),e.body.classList.remove(e.bodyActiveClass)):(e.body.classList.add(e.activeClass),e.body.classList.add(e.bodyActiveClass))}}]),t}();n(2);(o=jQuery)(document).ready((function(){o("body").removeClass("no-js"),(new c).init(),o('a[href^="#"]').click((function(t){t.preventDefault();var e=this.hash;o("html, body").stop().animate({scrollTop:o(e).offset().top-100+"px"},300)}))}))}]);