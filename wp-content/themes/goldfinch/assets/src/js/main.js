import toggle from './nav-toggle.js';
import './tabbing-navigation.js';

( function( $ ) {

  $( document ).ready( function() {

    $( 'body' ).removeClass( 'no-js' );

    let navToggle = new toggle();
    navToggle.init();

    $('a[href^="#"]').click( function(event) {
      event.preventDefault();

      // Get the current target hash
      var target = this.hash;

      // Animate the scroll bar action so its smooth instead of a hard jump
      $('html, body').stop().animate({
          'scrollTop' : $(target).offset().top - 100 + 'px'
      }, 300 );

  });

  });

})( jQuery );