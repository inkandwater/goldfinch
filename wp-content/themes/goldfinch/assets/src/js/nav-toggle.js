/**
 * toggle.js
 *
 * Handles toggling off-screen navigation
 *
 */
export default class navigation {

  constructor() {

    this.settings = {
      toggle      : document.getElementById( 'navigation-toggle' ),
      body        : document.getElementsByTagName( 'body' )[0],
      activeClass : 'js-nav-open',
      bodyActiveClass: 'body--inactive'
    }

  }

  init() {

    if ( null !== this.settings.toggle ) {
      this.bindUIActions();
    }

  }

  bindUIActions() {

    let s = this.settings;

    window.addEventListener( 'resize', () => {
      if ( window.innerWidth > 899 ) {
        this.activeClass( 'remove' );
      }
    } )

    s.toggle.addEventListener( 'click', () => {
      this.activeClass();
    });

  }

  activeClass( status ) {

    let s = this.settings;

    if ( s.body.classList.contains( s.activeClass ) ) {
      s.body.classList.remove( s.activeClass );
      s.body.classList.remove( s.bodyActiveClass );
    } else if ( 'remove' === status ) {
      s.body.classList.remove( s.activeClass );
      s.body.classList.remove( s.bodyActiveClass );
    } else {
      s.body.classList.add( s.activeClass );
      s.body.classList.add( s.bodyActiveClass );
    }

  }

}