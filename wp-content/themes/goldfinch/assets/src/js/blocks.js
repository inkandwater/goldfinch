wp.domReady( () => {

  wp.blocks.unregisterBlockType( 'core/verse' );
  wp.blocks.unregisterBlockType( 'core/audio' );
  wp.blocks.unregisterBlockType( 'core/code' );
  wp.blocks.unregisterBlockType( 'core/html' );
  wp.blocks.unregisterBlockType( 'core/preformatted' );

  wp.blocks.unregisterBlockStyle( 'core/image', 'circle-mask' );
  wp.blocks.unregisterBlockStyle( 'core/quote', 'default' );
  wp.blocks.unregisterBlockStyle( 'core/quote', 'plain' );
  wp.blocks.unregisterBlockStyle( 'core/separator', 'dots' );

  wp.blocks.registerBlockStyle( 'core/group', {
      name: 'overlap',
      label: 'Overlap',
  } );

  wp.blocks.registerBlockStyle( 'core/paragraph', {
      name: 'subheading',
      label: 'Subheading',
  } );

} );