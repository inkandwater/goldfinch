<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- head -->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="description" content="Goldfinch create ethical furniture of outstanding quality and provenance.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#cf4830" />

    <link rel="preload" href="https://use.typekit.net/ijf4bcz.css" as="style">
    <link rel="stylesheet" href="https://use.typekit.net/ijf4bcz.css">
    <link rel="preconnect" href="https://use.typekit.net" crossorigin>

    <?php goldfinch\touch_icon(); ?>
    <?php goldfinch\site_icon(); ?>

    <?php wp_head(); ?>

</head>
<!-- /head -->
<!-- body -->
<body class="wp-embed-responsive template page-template--under-contruction">

    <?php goldfinch\svg_defs(); ?>

    <div id="page" class="hfeed site">

        <section class="content-wrapper main">
            <?php goldfinch\branding(); ?>
            <h1 class="page-heading mt-2 h3"><?php echo __( "Custom made furniture", "goldfinch" ); ?></h1>

            <div class="page-content">
                <p class="mt-3">Goldfinch create ethical furniture of outstanding quality and provenance. We take great pride in our craft because we understand the intrinsic role furniture plays in our homes and businesses.</p>
                <p class="mt-6"><strong>Our new website is coming soon!</strong></p>

        </section>

    </div>

</body>
<!-- scripts -->
<?php wp_footer(); ?>
<!-- /scripts -->