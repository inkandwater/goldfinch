<?php

/**
 * Serve up a new Goldfinch!
 *
 * @package goldfinch
 * @since   1.0.0
 */

/**
 * Assign the theme version to a var
 */
$theme              = wp_get_theme( 'goldfinch' );
$theme_version      = $theme['Version'];

require 'inc/goldfinch-template-tags.php';
require 'inc/goldfinch-template-hooks.php';
require 'inc/goldfinch-template-functions.php';
require 'inc/goldfinch-functions.php';

require 'inc/class-goldfinch-nav-walker.php';
require 'inc/class-goldfinch-templating.php';
require 'inc/class-goldfinch-customizer.php';
require 'inc/class-goldfinch.php';

$startup = new Goldfinch();
$startup->init();

$customizer = new Goldfinch_Customizer();
$customizer->customizer_init();

$customizer = new Goldfinch_Templating();
$customizer->templating_init();

//REMOVE GUTENBERG BLOCK LIBRARY CSS FROM LOADING ON FRONTEND
function remove_wp_block_library_css(){
    //wp_dequeue_style( 'wp-block-library' );
    //wp_dequeue_style( 'wp-block-library-theme' );
//    wp_dequeue_style( 'global-styles' ); // REMOVE THEME.JSON
    }
    add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );