<?php

/**
 * Goldfinch Templating Class
 *
 * @package goldfinch
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Goldfinch_Templating' ) ) :

    /**
     * The goldfinch templating class
     */
    class Goldfinch_Templating {

        /**
         * Templating init
         * 
         * @since 1.0.0
         */
        public function templating_init() {

            add_action( 'wp_head',                    array( $this, 'page_templating' ),           10 );

        }

        /**
         * Front Page
         * 
         * @since 1.0.0
         */
        public function page_templating() {

            if ( is_page_template( 'templates/tpl-no-header.php' ) ) :

                remove_action( 'goldfinch_page',           'goldfinch\\page_header',              10 );

            endif;

        }


    }

endif;