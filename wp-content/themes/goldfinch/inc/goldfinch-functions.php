<?php

/**
 * Goldfinch Functions
 *
 * @package goldfinch
 * @since   1.0.0
 */

namespace goldfinch;

if ( ! function_exists( __NAMESPACE__ . '\custom_body_classes' ) ) :

    function custom_body_classes( $classes ) {

        global $post;
        $light_header = get_post_meta( get_the_ID(), '_gf_header_theme_light', true );

        $classes = [ 'wp-embed-responsive' ];

        if ( is_user_logged_in() ) {
            $classes[] = 'is-logged-in';
        }

        if ( is_front_page() ) {
            $classes[] = 'is-front-page';
            $classes[] = "template--homepage";
        }

        if ( is_404() ) {
            $classes[] = 'template--error404';
        }

        if ( is_search() ) {
            $classes[] = 'template--search';
        }

        if ( is_page() || is_home() || is_archive() || is_search() ) {
            $classes[] = "template";
        }

        if ( is_category() ) {
            $classes[] = "template--blog";
            $classes[] = "template--category";
        }

        if ( is_tag() ) {
            $classes[] = "template--blog";
            $classes[] = "template--tag";
        }

        if ( is_archive() ) {
            $classes[] = "template--archive";
        }

        if ( is_tax() ) {
            $classes[] = "template--tax";
        }

        if ( is_tax( 'post-author' ) ) {
            $classes[] = "template--author";
        }

        if ( is_home() ) {
            $classes[] = "template--blog";
            $classes[] = "template--blog-home";
        }

        if ( is_paged() ) {
            $classes[] = "template--paged";
        }

        if ( 'post' == get_post_type() && is_single() ) {
            $classes[] = 'template--single-post';
        }

        if ( is_page() && !is_page_template() ) {
            $classes[] = "template--default";
        }

        if ( has_post_thumbnail() && is_page() ) {
            $classes[] = 'template--has-image';
        }

        if ( $light_header && !is_search() ) {
            $classes[] = 'template--light-header';
        }

        if ( has_block( 'goldfinch/content-carousel' ) || has_block( 'goldfinch/testimonials' ) ) {
            $classes[] = 'template--overflow-hidden';
        }

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\custom_post_classes' ) ) :

    /**
     * Customise post classes
     *
     * @since  1.0.0
     */
    function custom_post_classes( $classes ) {

        $classes = array();

        if( is_front_page() ) :
            $classes[] = 'home';
        endif;

        if( has_post_thumbnail() ) :
            $classes[] = 'has-featured-image';
        endif;

        if( 'post' == get_post_type() && !is_single() ) :

            $classes = array( 'post', 'card', 'card--post', 'hentry' );

            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }

        endif;

        if( 'post' == get_post_type() && is_single() ) :

            $classes = array( 'post', 'post--single', 'hentry' );

            if( has_post_thumbnail() ) {
                $classes[] = 'post--has-featured-image';
            }

        endif;

        array_push( $classes, 'hentry' );
        array_push( $classes, get_post_type() );

        return $classes;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_header_class' ) ) :

    /**
     * Get classes for site header
     *
     * This function is based on WP's get_body_class.
     *
     * @param  string|string[] $class Space-separated string or array of class names to add to the class list.
     * @return string[] Array of class names.
     *
     * @since 1.0.0
     */
    function get_header_class( $class = '' ) {

        $classes = array( 'header' );

        // E.g.
        if( is_page_template( 'templates/tpl-no-header.php' ) ) :
            $classes[] = 'header--floating';
        endif;

        if ( ! empty( $class ) ) {

            if ( ! is_array( $class ) ) {
                $class = preg_split( '#\s+#', $class );
            }

            $classes = array_merge( $classes, $class );

        } else {
            // Ensure that we always coerce class to being an array.
            $class = array();
        }

        $classes = array_map( 'esc_attr', $classes );

        return array_unique( $classes );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_social_links' ) ) :

    /**
     * Get social media links
     *
     * @since  1.0.0
     */
    function get_social_links() {

        $social_links = array(
            'facebook'      => __( 'Facebook',  'goldfinch' ),
            'instagram'     => __( 'Instagram', 'goldfinch' ),
            'linkedin'      => __( 'LinkedIn',  'goldfinch' ),
            'pinterest'     => __( 'Pinterest', 'goldfinch' ),
            'twitter'       => __( 'Twitter',   'goldfinch' )
        );

        $links  = '';

        foreach ( $social_links as $slug => $label ) {

            if( !empty( get_theme_mod( $slug ) ) ) {
                $links .= sprintf(
                    "<li class='social-link social-link--%s'>
                        <a href='%s' rel='nofollow noopener' target='_blank'>
                            <span class='screen-reader-text'>%s</span>
                            <svg viewBox='0 0 40 40'>
                                <use href='#icon-{$slug}' xlink:href='#icon-{$slug}'></use>
                            </svg>
                        </a>
                    </li>",
                    esc_attr( $slug ),
                    get_theme_mod( $slug ),
                    esc_html( $label )
                );
            }
        }

        return $links;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\filter_archive_title' ) ) :

    function filter_archive_title() {

        if ( is_home() ) {
            $title = get_the_title( get_option( 'page_for_posts' ) );
        } elseif ( is_category() ) {
            /* translators: Category archive title. 1: Category name */
            $title = single_cat_title( '', false );
        } elseif ( is_tag() ) {
            /* translators: Tag archive title. 1: Tag name */
            $title = single_tag_title( '', false );
        } elseif ( is_tax() ) {
            $title = single_term_title( '', false );
        } elseif ( is_author() ) {
            $page_id = get_queried_object_id();
            setup_postdata( $page_id );
            $title = get_the_author( $page_id );
        }

        return $title;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\filter_post_thumbnail_size' ) ) :

    function filter_post_thumbnail_size( $size ) {

        switch (true) {
            case is_home() || is_archive() :
                $size = 'rectangle';
                break;
            case is_single() :
                $size = 'wide';
                break;
            default:
                $size = 'large';
                break;
        }

        return $size;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\filter_archive_description' ) ) :

    function filter_archive_description( $description ) {

        $page_for_posts = get_option( 'page_for_posts' );

        if ( is_home() && has_excerpt( $page_for_posts  ) ) {
            $description = get_the_excerpt( $page_for_posts  );
        }

        return $description;

    }

endif;

if ( ! function_exists( 'cover_block_render' ) ) :

    /**
     * Cover block render
     *
     * @since  1.0.0
     */
    function cover_block_render( $block_content, $block ) {

        if ( "core/cover" !== $block['blockName'] ) {
            return $block_content;
        }

        $to_insert = '<div class="wp-block-cover__inner-container"><a class="wp-block-cover__nav-arrow" href="#first-section" aria-label="next section"><svg viewBox="0 0 15 15" role="img" focusable="false" aria-hidden="true"><use href="#icon--arrow-down" xlink:href="#icon--arrow-down"></use></svg></a>';
        $block_content = str_replace( '<div class="wp-block-cover__inner-container">', $to_insert, $block_content );

        return $block_content;

    }

endif;