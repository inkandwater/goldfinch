<?php

/**
 * Goldfinch Customizer Class
 *
 * @package goldfinch
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Goldfinch_Customizer' ) ) :

    /**
     * The Goldfinch Customizer class
     */
    class Goldfinch_Customizer {


        public function __construct() {

        }

        public function customizer_init() {

            add_action( 'customize_register', array( $this, 'social_media_controls' ), 10 );

            // Enqueue live preview javascript in Theme Customizer admin screen
            add_action( 'customize_preview_init' , array( $this, 'live_preview' ) );

        }

        /**
         * Add in social media controls
         *
         * @since 1.0.0
         */
        public function social_media_controls( $wp_customize ) {

            $social_links = array(
                'facebook'      => __( 'Facebook', 'goldfinch' ),
                'instagram'     => __( 'Instagram', 'goldfinch' ),
                'linkedin'      => __( 'LinkedIn', 'goldfinch' ),
                'pinterest'     => __( 'Pinterest', 'goldfinch' ),
                'twitter'       => __( 'Twitter', 'goldfinch' )
            );

            $wp_customize->add_section( 'social-media', array(
                'title'             => __( 'Social Media', 'goldfinch' ),
                'priority'          => 100,
                'description'       => __( "<p>Add social media profile/page URL's.</p><p>These are used by the theme to display social media icon links.</p>", 'goldfinch' ),
            ) );

            foreach( $social_links as $key => $value ) {

                $wp_customize->add_setting( $key, array(
                    'sanitize_callback' => 'esc_url_raw',
                ) );

                $wp_customize->add_control( $key, array(
                    'label'             => $value,
                    'description'       => __( 'Add a social media URL', $this->text_domain ),
                    'section'           => 'social-media',
                    'type'              => 'text',
                    'priority'          => 99,
                ) );

            }

        }

        public function live_preview() {

        }

    }

endif;