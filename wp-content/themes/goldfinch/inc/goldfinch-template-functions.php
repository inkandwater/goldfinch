<?php

/**
 * Goldfinch template functions.
 *
 * @package goldfinch
 * @since   1.0.0
 */
namespace goldfinch;

if ( ! function_exists( __NAMESPACE__ . '\touch_icon' ) ) :

    function touch_icon() { ?>

        <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri( 'assets/build/img/favicon.svg' ); ?>">

    <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_icon' ) ) :

    function site_icon() { ?>

        <link rel="icon" type="image/svg+xml" href="<?php echo get_theme_file_uri( 'assets/build/img/favicon.svg' ); ?>">

    <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\svg_defs' ) ) :

    function svg_defs() {
        ?>
        <span class="svg-defs">
            <?php include_once( get_theme_file_path( '/assets/build/img/svg/sprite.svg' ) ); ?>
        </span>

    <?php
    }

endif;

/**
 * Header Functions.
 *
 * @package goldfinch
 * @since   1.0.0
 * @see     goldfinch_site_before
 * @see     goldfinch_header_before
 * @see     goldfinch_header
 * @see     goldfinch_content_before
 * @see     goldfinch_content_top
 */
if ( ! function_exists( __NAMESPACE__ . '\branding' ) ) :
    /**
     * Site branding wrapper and display
     */
    function branding() {
        ?>

        <!-- branding -->
        <div class="branding branding--header">
            <a class="branding__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" aria-label="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
                <svg viewBox="0 0 150 25" class="logo" role="img" focusable="false" aria-hidden="true">
                    <use href="#goldfinch-logo" xlink:href="#goldfinch-logo"></use>
                </svg>
            </a>
        </div>
        <!-- /branding -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\main_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function main_navigation() {

        if( has_nav_menu( 'main-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="main-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'goldfinch' ); ?>">
                <?php main_menu(); ?>
            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\navigation_toggle' ) ) :
    /**
     * Site branding wrapper and display
     */
    function navigation_toggle() {

        if( has_nav_menu( 'small-screen-menu' ) ) : ?>

            <button id="navigation-toggle" class="nav-toggle" aria-label="<?php _e( 'Small screen navigation toggle', 'symbio' ); ?>" aria-controls="small-screen-navigation">
                <span class="nav-toggle__text">Menu</span>
                <svg viewBox="0 0 15 15" class="nav-toggle__icon">
                    <use class="menu__open" href="#icon--menu" xlink:href="#icon--menu"></use>
                    <use class="menu__close" href="#icon--menu-close" xlink:href="#icon--menu-close"></use>
                </svg>
            </button>

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\small_screen_navigation' ) ) :
    /**
     * Site branding wrapper and display
     */
    function small_screen_navigation() {

        if( has_nav_menu( 'small-screen-menu' ) ) : ?>

            <!-- main-navigation -->
            <nav id="small-screen-navigation" class="small-screen-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'goldfinch' ); ?>">
                <?php small_screen_menu(); ?>
            </nav>
            <!-- /main-navigation -->

        <?php endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_credit' ) ) :

    function site_credit() { ?>

        <div class="site-credit">

            <?php printf( '<span class="credit">%s</span><a class="credit__link external" href="%s" rel="index">%s</a>',
                __( 'Design By ', 'goldfinch' ),
                esc_url( 'https://inkandwater.co.uk' ),
                __( 'Ink & Water', 'goldfinch' )
            ); ?>

        </div>

    <?php
    }

endif;

/**
 * Footer Functions.
 *
 * @package goldfinch
 * @since   1.0.0
 * @see     goldfinch_content_bottom
 * @see     goldfinch_footer_before
 * @see     goldfinch_footer
 * @see     goldfinch_footer_after
 */

if ( ! function_exists( __NAMESPACE__ . '\branding_footer' ) ) :

    function branding_footer() {
        ?>

        <!-- site-branding -->
        <div class="footer__logo branding branding--footer">
            <h1><?php echo get_bloginfo( 'name' ); ?></h1>
            <h2><?php echo get_bloginfo( 'description' ); ?></h2>
        </div>
        <!-- /site-branding -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_map' ) ) :

    function site_map() {
        if( has_nav_menu( 'site-map' ) ) : ?>

        <!-- site map -->
        <div class="site-map footer__nav-section footer__nav-section--half">
            <p class="text--bold text--large">Explore</p>
            <?php site_map_menu(); ?>
        </div>
        <!-- /site map -->

        <?php endif;
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_social' ) ) :

    function site_social() {
        ?>

        <!-- site map -->
        <div class="site-social footer__nav-section footer__nav-section--half">
            <p class="text--bold text--large">Social</p>
            <ul>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Instagram</a></li>
            </ul>
        </div>
        <!-- /site map -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_signup' ) ) :

    function site_signup() {
        ?>

        <!-- site map -->
        <div class="site-signup footer__nav-section">
            <p class="text--bold text--large">Subscribe</p>
            <ul>
                <li><a href="#">Sign up to our Newsletter</a></li>
            </ul>
        </div>
        <!-- /site map -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_legal' ) ) :

    function site_legal() {
        if( has_nav_menu( 'legal' ) ) : ?>

        <!-- legal -->
        <div class="site-legal">
            <?php legal_menu(); ?>
        </div>
        <!-- /legal -->

        <?php endif;
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_address' ) ) :
    /**
     * Site Address
     *
     * @since  1.0.0
     */
    function site_address() {
        ?>
            <div class="site-address footer__nav-section">
                <p class="text--bold text--large">Find Us</p>
                <address>
                    <p>Unit 2, 7 Spa Road<br>Bermondsey<br>London<br>SE163QP</p>
                    <p>020 7237 7831<br>
                    <a href="mailto:hello@goldfinchfurniture.co.uk">hello@goldfinchfurniture.co.uk</a></p>
                </address>
            </div>
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_copyright' ) ) :
    /**
     * Copyright info
     *
     * @since  1.0.0
     */
    function site_copyright() { ?>

        <div class="site-copyright">

            <?php printf( '<ul class="copyright-list"><li class="copyright-list__item">%s <a href="%s" rel="index">%s</a>.</li><li class="copyright-list__item">%s</li></ul>',
                __( '&copy; ', 'goldfinch' ) . ' ' . date('Y'),
                esc_url( home_url() ),
                esc_attr__( get_bloginfo( 'name' ) ),
                __( 'Charity No: 1148279. Company No: 07768745.', 'goldfinch' )
            ); ?>

        </div>

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '\social_links' ) ) :

    function social_links() {
        ?>

        <!-- site-social -->
        <div class="site-social">
            <?php printf( '<span class="site-social__heading">%s</span>', __( "Social", 'goldfinch' ) ); ?>
            <ul class="site-social__links">
                <?php echo get_social_links(); ?>
            </ul>
        </div>
        <!-- /site-social -->

        <?php
    }

endif;

/**
 * Post Functions.
 *
 * @package goldfinch
 * @since   1.0.0
 * @see     goldfinch_loop_before
 * @see     goldfinch_loop_after
 * @see     goldfinch_single_before
 * @see     goldfinch_single_post_top
 * @see     goldfinch_single_post
 * @see     goldfinch_single_post_bottom
 * @see     goldfinch_single_after
 */
if ( ! function_exists( __NAMESPACE__ . '\loop_wrapper' ) ) :

    function loop_wrapper() {

        $classes[] = 'posts--main';

        printf( '<div class="%s">', esc_attr( implode(' ', $classes ) ) );

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\loop_wrapper_close' ) ) :

    function loop_wrapper_close() {
        echo '</div>';
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_header' ) ) :
    /**
     * Display the post header with a link to the single post
     *
     * @since 1.0.0
     */
    function post_header() {
        ?>

        <!-- entry-header -->
        <header class="post__header entry-header">

            <aside class="post__meta entry-meta">
                <?php posted_on( get_the_ID() ); ?>
            </aside>

            <?php the_title( sprintf( '<h2 class="post__title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\single_post_header' ) ) :
    /**
     * Display the post header with a link to the single post
     *
     * @since 1.0.0
     */
    function single_post_header() {

        $page_id = get_the_ID();
        $thumbnail = get_the_post_thumbnail_url( $page_id, 'full' );
        $has_thumbnail = ( has_post_thumbnail($page_id) ) ? 'has-page-thumbnail' : null;
        $background_style = ( has_post_thumbnail($page_id) && is_single() ) ? sprintf( 'style="background-image: url(%s);"', esc_url( $thumbnail ) ) : null; ?>

        <!-- entry-header -->
        <header class="post__header entry-header <?php echo $has_thumbnail; ?>" <?php echo $background_style; ?>>
            <div class="post__header--inner">

                <?php the_title( '<h1 class="post__title entry-title heading--xl">', '</h1>' ); ?>

                <aside class="post__meta entry-meta">
                    <?php post_categories(false); ?>
                    <?php posted_on( get_the_ID() ); ?>
                </aside>

            </div>
        </header>
        <!-- /entry-header -->

    <?php }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_thumbnail' ) ) :
    /**
     * Display post thumbnail
     *
     * @var $size thumbnail size. thumbnail|medium|large|full|$custom
     * @uses has_post_thumbnail()
     * @uses the_post_thumbnail
     * @param string $size the post thumbnail size.
     * @since 1.5.0
     */
    function post_thumbnail( $size = 'full' ) {

        $size = apply_filters( 'goldfinch_post_thumbnail_size', $size );
        $count = get_query_var('count');

        if ( $count >= 4 )
            return;

        if ( has_post_thumbnail() && $count < 4 ) { ?>

            <!-- entry-thumbnail -->
            <figure class="post__thumbnail entry-thumbnail">
                <?php the_post_thumbnail( $size ); ?>
            </figure>
            <!-- /entry-thumnbail -->

        <?php }

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_content' ) ) :
    /**
     * Display the post content with a link to the single post
     *
     * @since 1.0.0
     */
    function post_content() {
        $class = ( is_single() )? 'entry-content' : 'entry-summary';
        ?>
            <!-- entry-content -->
            <div class="post__content <?php echo esc_attr( $class ); ?> content-block">
                <?php

                /**
                 * Functions hooked in to goldfinch_post_content_before action.
                 *
                 * @hooked goldfinch_post_thumbnail - 10
                 */
                do_action( 'goldfinch_post_content_before' );

                if( is_single() ) :
                    the_content(
                        sprintf(
                            __( 'Continue reading %s', 'goldfinch' ),
                            '<span class="screen-reader-text">' . get_the_title() . '</span>'
                        )
                    );
                else :
                    the_excerpt();
                endif;

                do_action( 'goldfinch_post_content_after' );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'goldfinch' ),
                    'after'  => '</div>',
                ) );
                ?>
            </div>
            <!-- /entry-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\paging_nav' ) ) :
    /**
     * Display navigation to next/previous set of posts when applicable.
     */
    function paging_nav() {
        global $wp_query;

        $args = array(
            'type'      => 'list',
            'next_text' => _x( 'Next', 'Next post', 'goldfinch' ),
            'prev_text' => _x( 'Previous', 'Previous post', 'goldfinch' ),
            );

        the_posts_pagination( $args );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_nav' ) ) :
    /**
     * Display navigation to next/previous post when applicable.
     */
    function post_nav() {

        $args = array(
            'prev_text' => __( '<span class="nav-arrow">Older</span> <span class="nav-previous-title">%title</span>', 'goldfinch' ),
            'next_text' => __( '<span class="nav-arrow">Newer</span> <span class="nav-next-title">%title</span>', 'goldfinch' ),
            );
        ?>

        <!-- post-navigation -->
        <?php the_post_navigation( $args ); ?>
        <!-- /post-navigation -->

        <?php

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\category_list' ) ) :

    function category_list() {

        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( esc_html__( ' ', 'goldfinch' ) );

        if ( $categories_list ) :
            printf( '<div class="categories-links"><span class="categories-label text--small">%s</span> %s</div>',
                esc_attr( __( 'View by:', 'goldfinch' ) ),
                wp_kses_post( $categories_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\tag_list' ) ) :

    function tag_list() {

        /* translators: used between list items, there is a space after the comma */
        $tags_list = get_the_tag_list( '', __( ', ', 'goldfinch' ) );

        if ( $tags_list ) :
            printf( '<div class="tags-links"><span class="tags-label">%s</span> %s</div>',
                esc_attr( __( 'Tagged', 'goldfinch' ) ),
                wp_kses_post( $tags_list )
            );
        endif; // End if $tags_list.

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $posted_on = sprintf( '<a href="%1s" rel="bookmark">%2s</a>', esc_url( get_permalink() ), $time_string );

        echo wp_kses( apply_filters( 'goldfinch_single_post_posted_on_html', '<div class="posted-on">' . $posted_on . '</div>', $posted_on ), array(
            'div' => array(
                'class'  => array(),
            ),
            'a'    => array(
                'href'  => array(),
                'title' => array(),
                'rel'   => array(),
            ),
            'time' => array(
                'datetime' => array(),
                'class'    => array(),
            ),
        ) );
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\posted_by' ) ) :

    function posted_by() {
        ?>
            <div class="author-meta">
                <?php printf( '<div class="avatar">%s</div>', get_avatar( get_the_author_meta( 'ID' ), 48 ) ); ?>
                <?php printf( '<b class="label">%s</b>', get_the_author_posts_link() ); ?>
            </div>

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_categories' ) ) :

    function post_categories( $show_title = false ) {

        /* translators: used between list items, there is a space after the comma */
        $categories = get_the_category( get_the_ID() );
        $title_class = ( ! $show_title ) ? 'screen-reader-text' : null;

        if ( !empty( $categories ) ) :
            ?>

            <div class="categories-links">
                <?php printf( '<span class="categories-label text--small %s">%s</span>', esc_html( $title_class  ), esc_attr( __( 'Categories:', 'goldfinch' ) ) ); ?>

                <ul class="categories">
                    <?php foreach ( $categories as $index => $category ) :
                        if ( $index <= 2 ) {
                            printf( '<li class="category text--small mb-1"><a href="%s">%s</a></li>', esc_url( get_category_link( $category->term_id ) ), esc_html( $category->name ) );
                        }
                    endforeach; ?>
                </ul>

            </div>

            <?php
        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_tags' ) ) :

    function post_tags( $show_title = false ) {

        /* translators: used between list items, there is a space after the comma */
        $tags = get_the_tags( get_the_ID() );
        $title_class = ( ! $show_title ) ? 'screen-reader-text' : null;

        if ( !empty( $tags ) ) : ?>

            <div class="tag-links">
                <?php printf( '<span class="tags-label text--small %s">%s</span>', esc_html( $title_class  ), esc_attr( __( 'Tags:', 'goldfinch' ) ) ); ?>

                <ul class="tags">
                    <?php foreach ( $tags as $index => $tag ) :
                        if ( $index <= 2 ) {
                            printf( '<li class="tag text--small mb-1"><a href="%s">%s</a></li>', esc_url( get_tag_link( $tag->term_id ) ), esc_html( $tag->name ) );
                        }
                    endforeach; ?>
                </ul>

            </div>

            <?php
        endif;

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\post_nav_link' ) ) :

    function post_nav_link() { ?>

        <div class="post__nav-link nav-link">
            <a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" class="post__more text--small" rel="bookmark">
            Find out more
            <svg viewbox="0 0 20 20"><use href="#icon--arrow-right"></use></svg>
            </a>
        </div>

    <?php }

endif;

/**
 * Page Functions.
 *
 * @package goldfinch
 * @since   1.0.0
 * @see     goldfinch_page_before
 * @see     goldfinch_page_after
 */
if ( ! function_exists( __NAMESPACE__ . '\page_header' ) ) :

    function page_header( $size = 'full' ) {

        $size = apply_filters( 'goldfinch_page_header_thumbnail_size', $size );
        ?>

        <!-- entry-header -->
        <header class="page__header entry-header">
            <div class="page__header-inner">
                <?php the_title( '<h1 class="page__title entry-title heading--xxl">', '</h1>' ); ?>
                <?php if( has_excerpt() ) :
                    printf( '<p class="page__intro has-neutral-400-color">%s</p>', get_the_excerpt() );
                endif; ?>
            </div>

        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\page_content' ) ) :

    function page_content() {
        ?>

        <!-- entry-content -->
        <div class="page__content entry-content content-block">
            <?php the_content(); ?>
            <?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'goldfinch' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div>
        <!-- /entry-content -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\get_sidebar' ) ) :
    /**
     * Display goldfinch sidebar
     *
     * @uses get_sidebar()
     * @since 1.0.0
     */
    function get_sidebar() {

        get_sidebar();

    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\search_header' ) ) :

    function search_header() { ?>

        <!-- entry-header -->
        <header class="search-result__header entry-header mb-1">
            <?php the_title( sprintf( '<h2 class="search-result__title entry-title mb-0"><a href="%s">', get_permalink( get_the_ID() ) ), '</a></h2>' ); ?>
            <?php printf( '<a class="search-result__permalink text--small mt-0" href="%1$s" rel="bookmark">%1$s</a>', get_the_permalink() ); ?>
        </header>
        <!-- /entry-header -->

        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\search_content' ) ) :

    function search_content() { ?>

        <?php if ( has_excerpt( get_the_ID() ) ) : ?>

            <main class="search-result__content entry-summary">
                <?php the_excerpt(); ?>
            </main>

        <?php endif; ?>

    <?php }

endif;

/**
 * 404 Functions.
 *
 * @package goldfinch
 * @since   1.0.0
 * @see     goldfinch_404_page_content
 */
if ( ! function_exists( __NAMESPACE__ . '\error_404_text' ) ) :

    function error_404_text() {
        ?>
            <!-- page-content -->
            <div class="page-content">
                <p class="mb-2 text--large"><?php esc_html_e( 'The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. To get back on track, try one of the links in the header, or search for what you need.', 'goldfinch' ); ?></p>
                <?php site_search(); ?>
            </div>
            <!-- /page-content -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\error_404_header' ) ) :

    function error_404_header() {
        ?>
            <!-- page-header -->
            <header class="page-header">
                <h1 class="page-title heading--xxl"><?php esc_html_e( 'Page Not Found', 'goldfinch' ); ?></h1>
            </header>
            <!-- /page-header -->
        <?php
    }

endif;

if ( ! function_exists( __NAMESPACE__ . '\site_search' ) ) :

    function site_search() {
        ?>
            <!-- site-search -->
            <section class="search-form-container" aria-label="Search">
                <?php get_search_form(); ?>
            </section>
            <!-- /site-search -->
        <?php
    }

endif;