<?php

/**
 * Set up Goldfinch theme
 *
 * @package goldfinch
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Goldfinch' ) ) :

    class Goldfinch {

        /**
         * Init
         *
         * @since 1.0.0
         */
        public function init() {

            add_action( 'after_setup_theme',                array( $this, 'setup' ) );
            add_action( 'after_setup_theme',                array( $this, 'cleanup' ) );
            add_action( 'after_setup_theme',                array( $this, 'register_menus' ) );
            add_action( 'wp_enqueue_scripts',               array( $this, 'styles' ) );
            add_action( 'admin_enqueue_scripts',            array( $this, 'admin_styles' ),                        10 );
            add_action( 'wp_enqueue_scripts',               array( $this, 'scripts' ) );
            add_action( 'wp_head',                          array( $this, 'resource_prefetch' ),                    1 );

            add_action( 'enqueue_block_editor_assets',      array( $this, 'admin_scripts' ) );
            add_action( 'map_meta_cap',                     array( $this, 'privacy_page_permissions' ),          1, 4 );
            add_action( 'after_switch_theme',               array( $this, 'create_content_on_activation' ),        99 );

            add_action( 'init',                             array( $this, 'remove_comment_support' ),             100 );
            add_action( 'admin_menu',                       array( $this, 'remove_admin_menus' ) );
            add_action( 'wp_before_admin_bar_render',       array( $this, 'admin_bar_render' ) );

            add_filter( 'auto_update_theme',                 array( $this, 'auto_update_settings' ),             10, 2 );
            add_filter( 'site_transient_update_themes',      array( $this, 'remove_update_themes'),              10, 1 );
            add_filter( 'image_size_names_choose',           array( $this, 'add_custom_sizes' ),                 10, 1 );

        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * @since  1.0.0
         */
        public function setup() {

            /**
             * Load text domain
             */
            load_theme_textdomain( 'goldfinch', get_template_directory() . '/languages' );

            /**
             * Let WordPress manage the document title.
             */
            add_theme_support( 'title-tag' );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails', array(
                'post',
                'page'
            ) );

            add_theme_support( 'automatic-feed-links' );

            /**
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption'
            ) );

            add_theme_support( 'responsive-embeds' );
            add_theme_support( 'align-wide' );

            add_post_type_support( 'page', 'excerpt' );

            add_image_size( 'wide', 1920, 700, true );
            add_image_size( 'square', 800, 800, true );
            add_image_size( 'rectangle', 600, 400, true );
            add_image_size( 'tall', 650, 900, true );
            add_image_size( 'slide', 1100, 640, true );

        }

        /**
         * Custom Image sizes
         *
         * @since 1.0.0
         */
        public function add_custom_sizes( $sizes ) {

            return array_merge( $sizes, array(
                'square'    => __( 'Square', 'goldfinch' ),
                'rectangle' => __( 'Rectangle', 'goldfinch' ),
                'tall'      => __( 'Tall', 'goldfinch' ),
                'wide'      => __( 'Wide', 'goldfinch' )
            ) );

        }

        /**
         * Removes comments from admin menu
         *
         * @since 1.0.0
         */
        public function remove_admin_menus() {
            remove_menu_page( 'edit-comments.php' );
        }

        /**
         * Removes comments from post and pages
         *
         * @since 1.0.0
         */
        public function remove_comment_support() {

            remove_post_type_support( 'post', 'comments' );
            remove_post_type_support( 'page', 'comments' );

        }

        /**
         * Removes comments from admin bar
         *
         * @since 1.0.0
         */
        public function admin_bar_render() {

            global $wp_admin_bar;
            $wp_admin_bar->remove_menu('comments');

        }

        /**
         * Turn off auto update on this theme to avoid
         * name conflicts.
         *
         * @since  1.0.0
         */
        public function auto_update_settings( $update, $item ) {

            // Array of theme slugs to never auto-update
            $themes = array (
                'goldfinch',
            );

            if ( in_array( $item->theme, $themes ) ) {
                return false; // never update themes in this array
            } else {
                return $update; // Else, use the normal API response to decide whether to update or not
            }

        }

        /**
         * Turn off auto update on this theme to avoid
         * name conflicts.
         *
         * @since  1.0.0
         */
        public function remove_update_themes( $value ) {

            // Set your theme slug accordingly:
            $your_theme_slug = 'goldfinch';

            if ( isset( $value ) && is_object( $value ) ) {
                unset( $value->response[ $your_theme_slug ] );
            }

            return $value;
        }

        /**
         * Create example content
         *
         * @since 1.0.0
         */
        public function create_content_on_activation() {

            $pages = [
                'home'     => __( 'Home', 'goldfinch' ),
                'about'    => __( 'About','goldfinch' ),
                'services' => __( 'Services', 'goldfinch' ),
                'blog'     => __( 'Blog', 'goldfinch' ),
                'contact'  => __( 'Contact', 'goldfinch' )
            ];

            foreach ( $pages as $slug => $title ) {

                $page_check = get_page_by_title( $title );

                $new_page = array(
                    'post_type'     => 'page',
                    'post_title'    => $title,
                    'post_status'   => 'publish',
                    'post_author'   => 1,
                    'post_name'     => $slug
                );

                if ( !isset( $page_check->ID ) ) {
                    $page_id = wp_insert_post( $new_page );
                }

            }

            $menus = [
                'main-menu'         => __( 'Main Menu', 'goldfinch' ),
                'site-map'          => __( 'Site Map', 'goldfinch' ),
                'legal'             => __( 'Legal', 'goldfinch' )
            ];

            foreach ( $menus as $location => $name ) {

                // Does the menu exist already?
                $menu_exists = wp_get_nav_menu_object( $name );

                // If it doesn't exist, let's create it.
                if( !$menu_exists ) :
                    $menu_id = wp_create_nav_menu( $name );

                    foreach ( $pages as $slug => $title ) :

                        wp_update_nav_menu_item( $menu_id, 0, array(
                            'menu-item-title'     => $title,
                            'menu-item-object'    => 'page',
                            'menu-item-object-id' => get_page_by_title( $title )->ID,
                            'menu-item-type'      => 'post_type',
                            'menu-item-status'    => 'publish' )
                        );

                    endforeach;

                    // Grab the theme locations and assign our newly-created menu
                    // to the BuddyPress menu location.
                    if( !has_nav_menu( $location ) ){
                        $locations = get_theme_mod( 'nav_menu_locations' );
                        $locations[$location] = $menu_id;
                        set_theme_mod( 'nav_menu_locations', $locations );
                    }

                endif;

            }

        }

        /**
         * Removes a bunch of default links, scripts and styles that
         * clutter up the <head> section.
         *
         * @since  1.0.0
         */
        public function cleanup() {

            remove_action( 'wp_head',         'rsd_link' );
            remove_action( 'wp_head',         'wlwmanifest_link' );
            remove_action( 'wp_head',         'wp_generator' );
            remove_action( 'wp_head',         'parent_post_rel_link' );
            remove_action( 'wp_head',         'start_post_rel_link' );
            remove_action( 'wp_head',         'index_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link' );
            remove_action( 'wp_head',         'adjacent_posts_rel_link_wp_head', 10, 0 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head' );
            remove_action( 'wp_head',         'feed_links',                      2 );
            remove_action( 'wp_head',         'feed_links_extra',                3 );
            remove_action( 'wp_head',         'wp_shortlink_wp_head',            10, 0 );
            remove_action( 'wp_head',         'print_emoji_detection_script',    7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );

        }

        /**
         * Registers menu locations for the theme.
         *
         * @since  1.0.0
         */
        public function register_menus() {

            register_nav_menus(
                array(
                    'main-menu'         => __( 'Main Menu', 'goldfinch' ),
                    'small-screen-menu' => __( 'Small Screen Menu', 'goldfinch' ),
                    'site-map'          => __( 'Site Map', 'goldfinch' ),
                    'legal'             => __( 'Legal', 'goldfinch' )
                )
            );

        }

        // /**
        //  * Color Palette for Editor
        //  *
        //  * @return Array array of colors with name, slug and hex value
        //  * @since  1.0.0
        //  */
        // public function color_palette() {

        //     add_theme_support( 'editor-color-palette', array(

        //         array(
        //             'name'  => __( 'Red', 'goldfinch' ),
        //             'slug'  => 'red',
        //             'color' => '#FF0000',
        //         ),
        //         array(
        //             'name'  => __( 'Green', 'goldfinch' ),
        //             'slug'  => 'green',
        //             'color' => '#00FF00',
        //         ),
        //         array(
        //             'name'  => __( 'Blue', 'goldfinch' ),
        //             'slug'  => 'blue',
        //             'color' => '#0000FF',
        //         )

        //     ) );

        // }

        /**
         * Registers and enqueues front end stylesheets
         *
         * @since  1.0.0
         */
        public function styles() {

            global $theme_version;

            wp_register_style(
                'goldfinch',
                get_stylesheet_uri(),
                '',
                time(),
                'all'
            );

            wp_enqueue_style(
                'fonts',
                'https://use.typekit.net/ijf4bcz.css',
                '',
                time(),
                'all'
            );

            wp_enqueue_style( 'goldfinch' );

        }

        /**
         * Registers and enqueues front end scripts
         *
         * @since  1.0.0
         */
        public function scripts() {

            global $theme_version;

            wp_register_script(
                'main',
                get_theme_file_uri( '/assets/build/js/main.js' ),
                array( 'jquery' ),
                time(),
                true
            );

            wp_enqueue_script( 'main' );

        }

        /**
         * Registers and enqueues admin stylesheets
         *
         * @since  1.0.0
         */
        public function admin_styles() {

            global $theme_version;

            // Register block editor styles for backend.
            wp_register_style(
                'admin', // Handle.
                get_theme_file_uri( 'assets/build/css/admin.css' ), // Block editor CSS.
                array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
                time(),
                'all'
            );

            wp_enqueue_style( 'admin' );

            wp_enqueue_style(
                'fonts',
                'https://use.typekit.net/ijf4bcz.css',
                '',
                $theme_version,
                'all'
            );

        }

        /**
         * Registers and enqueues admin scripts
         *
         * @since  1.0.0
         */
        public function admin_scripts() {

            global $theme_version;

            wp_register_script(
                'goldfinch-blocks',
                get_theme_file_uri( 'assets/build/js/blocks.js' ),
                 array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
                time(),
                false
            );

            wp_enqueue_script( 'goldfinch-blocks' );

        }

        /**
         * Prefetch font resources for performance
         *
         * @since  1.0.0
         */
        public function resource_prefetch() { ?>

            <link rel="preconnect" href="https://use.typekit.net" crossorigin>

        <?php }

        /**
         * Allow editors to manage privacy page
         *
         * @since  1.0.0
         */
        public function privacy_page_permissions( $caps, $cap, $user_id, $args ) {

            if ( 'manage_privacy_options' === $cap ) {
                $manage_name = is_multisite() ? 'manage_network' : 'manage_options';
                $caps = array_diff($caps, [ $manage_name ]);
            }

            return $caps;

        }

    }

endif;