<?php

/**
 * Goldfinch hooks
 *
 * @package goldfinch
 * @since   1.0.0
 */

/**
 * General
 *
 */
add_action( 'goldfinch_site_before',             'goldfinch\\svg_defs',                    10 );
add_filter( 'body_class',                        'goldfinch\\custom_body_classes',         10, 1 );
add_filter( 'post_class',                        'goldfinch\\custom_post_classes',         10, 1 );
add_filter( 'render_block',                      'goldfinch\\cover_block_render',          10, 3 );
add_filter( 'get_the_archive_description',       'goldfinch\\filter_archive_description',   10, 1 );
add_filter( 'get_the_archive_title',             'goldfinch\\filter_archive_title',         10, 1 );
add_filter( 'goldfinch_post_thumbnail_size',     'goldfinch\\filter_post_thumbnail_size',   10, 1 );

add_action( 'wp_head',                          'goldfinch\\site_icon',                   10, 1 );

/**
 * Header
 *
 * @see goldfinch_header_before
 * @see goldfinch_header
 * @see goldfinch_content_top
 */
add_action( 'goldfinch_header',                   function(){ echo '<div class="header__inner">'; }, 10 );
add_action( 'goldfinch_header',                   'goldfinch\\branding',                 10 );
add_action( 'goldfinch_header',                   'goldfinch\\main_navigation',          20 );
add_action( 'goldfinch_header',                   'goldfinch\\small_screen_navigation',  20 );
add_action( 'goldfinch_header',                   'goldfinch\\navigation_toggle',        30 );
add_action( 'goldfinch_header',                   function(){ echo '</div>'; },          40 );

/**
 * Footer
 *
 * @see goldfinch_footer_before
 * @see goldfinch_footer
 * @see goldfinch_footer_after
 */
add_action( 'goldfinch_footer',                   function(){ echo '<div class="footer__inner footer__inner--flex">'; }, 10 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_address',             20 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_map',                 30 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_social',              40 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_signup',              50 );
add_action( 'goldfinch_footer',                   function(){ echo '</div>'; },          60 );
add_action( 'goldfinch_footer',                   function(){ echo '<div class="footer__inner">'; }, 70 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_legal',               80 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_copyright',           90 );
add_action( 'goldfinch_footer',                   'goldfinch\\site_credit',              100 );
add_action( 'goldfinch_footer',                   function(){ echo '</div>'; },          110 );

/**
 * Pages
 *
 * @see goldfinch_page
 */
add_action( 'goldfinch_page',                     'goldfinch\\page_header',                10 );
add_action( 'goldfinch_page',                     'goldfinch\\page_content',               20 );

add_action( 'goldfinch_search_before',            function(){ echo '<div class="search-results__posts">'; }, 10 );
add_action( 'goldfinch_search',                   'goldfinch\\search_header',                30 );
add_action( 'goldfinch_search',                   'goldfinch\\search_content',               40 );
add_action( 'goldfinch_search_after',             function(){ echo '</div>'; },              10 );
add_action( 'goldfinch_search_after',             'goldfinch\\paging_nav',                   30 );

/**
 * Posts
 *
 * @see goldfinch_loop_before
 * @see goldfinch_loop_post
 * @see goldfinch_loop_after
 * @see goldfinch_single_post
 * @see goldfinch_single_after
 * @see goldfinch_single_post_bottom
 */
add_action( 'goldfinch_loop_before',              'goldfinch\\loop_wrapper',               20 );
add_action( 'goldfinch_loop_post',                'goldfinch\\post_thumbnail',             20 );
add_action( 'goldfinch_loop_post',                function() { echo "<div class='card__body'>"; }, 30 );
add_action( 'goldfinch_loop_post',                'goldfinch\\post_header',                40 );
add_action( 'goldfinch_loop_post',                'goldfinch\\post_content',               50 );
add_action( 'goldfinch_loop_post',                function() { echo "</div>"; },           60 );
add_action( 'goldfinch_loop_post',                'goldfinch\\post_nav_link',              70 );
add_action( 'goldfinch_loop_after',               'goldfinch\\loop_wrapper_close',         10 );
add_action( 'goldfinch_loop_after',               'goldfinch\\paging_nav',                 20 );

add_action( 'goldfinch_single_post',              'goldfinch\\single_post_header',         10 );
add_action( 'goldfinch_single_post',              'goldfinch\\post_content',               30 );
add_action( 'goldfinch_single_after',             'goldfinch\\post_nav',                   10 );

/**
 * 404
 *
 * @see goldfinch_404_page_content
 */
add_action( 'goldfinch_404_page_content',         'goldfinch\\error_404_header',           10 );
add_action( 'goldfinch_404_page_content',         'goldfinch\\error_404_text',             20 );