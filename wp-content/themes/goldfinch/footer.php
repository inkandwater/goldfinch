<?php

/**
 * The template for displaying the footer.
 *
 * @package goldfinch
 * @since   1.0.0
 */

/**
 * Functions hooked into goldfinch_content_bottom
 *
 */
do_action( 'goldfinch_content_bottom' ); ?>

    <?php
    /**
     * Functions hooked into goldfinch_footer_before
     *
     * @see 10 goldfinch_footer_bar
     */
    do_action( 'goldfinch_footer_before' ); ?>

    <!-- footer -->
    <footer id="footer" class="footer">
        <?php
        /**
         * Functions hooked into goldfinch_footer
         *
         * @see 10 goldfinch_footer_widgets
         * @see 20 goldfinch_site_terms
         */
        do_action( 'goldfinch_footer' ); ?>
    </footer>
    <!-- /footer -->

    <?php
    /**
     * Functions hooked into goldfinch_footer_before
     *
     * @see 10 goldfinch_off_canvas_close
     */
    do_action( 'goldfinch_footer_after' ); ?>

</div>

<!-- scripts -->
<?php wp_footer(); ?>
<!-- /scripts -->

</body>
</html>