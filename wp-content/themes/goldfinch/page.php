<?php

/**
 * The template for displaying all pages.
 *
 * @package goldfinch
 * @since   1.0.0
 */

get_header(); ?>

    <!-- content-area -->
    <section class="content-area">

        <?php while ( have_posts() ) : the_post();

            /**
             * Functions hooked into goldfinch_page_before
             *
             */
            do_action( 'goldfinch_page_before' );

            get_template_part( 'template-parts/content', 'page' );

            /**
             * Functions hooked into goldfinch_page_after
             *
             */
            do_action( 'goldfinch_page_after' );

        endwhile; ?>

    </section>

<?php
get_footer();