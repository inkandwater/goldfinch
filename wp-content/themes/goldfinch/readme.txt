=== Goldfinch ===

Contributors: tomnapier, paulrmyers
Requires at least: 5.9.0
Tested up to: 5.9.2
Stable tag: 1.0.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Goldfinch is a bespoke theme for Goldfinch furniture

== Changelog ==

* 1.0.0 *

- Initial release.
