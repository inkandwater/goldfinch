<?php

/**
 * The template for displaying archive pages.
 *
 * @package bias
 * @since   1.0.0
 */

get_header();

$page_id = get_option( "page_for_posts" );
$thumbnail = get_the_post_thumbnail_url( $page_id, 'full' );
$has_thumbnail = ( has_post_thumbnail($page_id) ) ? 'has-page-thumbnail' : null;
$background_style = ( has_post_thumbnail($page_id) ) ? sprintf( 'style="background-image: url(%s);"', esc_url( $thumbnail ) ) : null; ?>

    <!-- content-area -->
    <section class="content-area">

        <!-- page-header -->
        <header class="page__header <?php echo $has_thumbnail; ?>" <?php echo $background_style; ?>>

            <div class="page__header-inner">
                <?php
                printf( '<h2 class="latest-posts-link"><a href="%s" rel="link for blog">%s</a></h2>', esc_url( get_permalink( $page_id ) ), esc_html( get_the_title( $page_id ) ) );
                the_archive_title( '<h1 class="page__title heading--xxl">', '</h1>' );
                the_archive_description( '<p class="page__description mt-0">', '</p>' );
                goldfinch\category_list(); ?>
            </div>

        </header>
        <!-- /page-header -->

        <?php if ( have_posts() ) : ?>

            <?php get_template_part( 'loop' );

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif; ?>

    </section>

<?php
get_footer();