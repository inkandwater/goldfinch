<?php
/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Lightbox
 * @subpackage Goldfinch_Lightbox/includes
 * @author     Ink & Water Ltd <paul@inkandwater.co.uk>
 */

class Goldfinch_Lightbox_i18n {

    public function load_plugin_textdomain() {

        load_plugin_textdomain(
            'goldfinch-lightbox',
            false,
            dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
        );

    }

}
