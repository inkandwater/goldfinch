<?php
/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Lightbox
 * @subpackage Goldfinch_Lightbox/includes
 * @author     Ink & Water Ltd <paul@inkandwater.co.uk>
 */

class Goldfinch_Lightbox_Deactivator {

    public static function deactivate() {}

}
