const { addFilter } = wp.hooks;
const { createHigherOrderComponent } = wp.compose;

//restrict to specific block names
const allowedBlocks = [ 'core/image' ];

/**
 * Add new valid attributes to the image block.
 *
 * @param  {[type]} props [description]
 * @param  {[type]} name  [description]
 * @return {[type]}       [description]
 */
const addDataAttributes = ( settings ) => {

   //check if object exists for old Gutenberg version compatibility
  //add allowedBlocks restriction
  if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){
      settings.attributes = Object.assign( settings.attributes, {
        imageWidth: {
          type: 'number',
          default: '',
        },
        imageHeight: {
          type: 'number',
          default: '',
        }
      }
    )
  }

  return settings;

};

addFilter( 'blocks.registerBlockType', 'goldfinchLightbox/attributes/data', addDataAttributes );


/**
 * Set additional image data attributes
 * @param  {[type]} ( BlockEdit     ) [description]
 * @return {[type]}   [description]
 */
const withImageData = createHigherOrderComponent( ( BlockEdit ) => {

  return ( props ) => {
    if ( 'core/image'  !== props.name ) {
      return <BlockEdit { ...props } />;
    }

    const { attributes, setAttributes } = props;

    const imageId  = attributes.id;
    const imageObj = wp.data.select('core').getMedia( imageId );

    if( imageObj ) {
      setAttributes( {
        "imageWidth": imageObj.media_details.width,
        "imageHeight": imageObj.media_details.height,
        "imageCaption": imageObj.caption
      } )
    }

    return <BlockEdit { ...props } />;

  };

});

addFilter( 'editor.BlockEdit', 'goldfinchLightbox/attributes/data', withImageData );

/**
 * Add data attributes to the image element
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
 const addImageDataAttributes =  ( saveElementProps, blockType, attributes ) => {

  // Do nothing if it's another block than our defined ones.
  if ( ! allowedBlocks.includes( blockType.name ) ) {
      return saveElementProps;
  }

  // Use Lodash's assign to gracefully handle if attributes are undefined
  assign( saveElementProps, {
    "data-width": attributes.imageWidth,
    "data-height": attributes.imageHeight
  } );

  if( '' !== attributes.imageHeight || attributes.imageWidth ) {
    assign( saveElementProps, {
      "data-lightbox": true
    } );
  }

  return saveElementProps;

};

addFilter( 'blocks.getSaveContent.extraProps', 'goldfinchLightbox/getSaveContent', addImageDataAttributes );