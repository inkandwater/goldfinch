import './photoswipe.scss';

const parseElements = ( elements ) => {

    let items = [];

    for ( let i = 0; i < elements.length; i++ ) {

      let el = elements[i];

      let imgSrc = el.href;

      if ( '' !== imgSrc ) {

        let width  = el.parentNode.dataset.width;
        let height = el.parentNode.dataset.height;

        let item = {
            src: imgSrc,
            w: parseInt(width),
            h: parseInt(height),
            pid: el.parentNode.dataset.pswpUid
        };

        if( el.parentNode.querySelector( 'figcaption' ) ) {
          item.title = el.parentNode.querySelector( 'figcaption' ).innerText;
        }

        items.push( item );

      }

    }

    return items;

}

// parse picture index and gallery index from URL (#&pid=1&gid=2)
const photoswipeParseHash = () => {

  let hash = window.location.hash.substring(1),
  params = {};

  if( hash.length < 5 ) {
    return params;
  }

  let vars = hash.split('&');

  for (let i = 0; i < vars.length; i++) {
    if( !vars[i] ) {
      continue;
    }
    let pair = vars[i].split('=');  
    if( pair.length < 2 ) {
      continue;
    }           
    params[pair[0]] = pair[1];
  }

  if(params.gid) {
      params.gid = parseInt(params.gid, 10);
  }

  return params;

};

let galleryElements = document.querySelectorAll( '.wp-block-image a' );

// find nearest parent element
let closest = function closest(el, fn) {
  return el && ( fn(el) ? el : closest(el.parentNode, fn) );
};

const openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {

    let pswpElement = document.querySelectorAll('.pswp')[0],
        gallery,
        options,
        items;

    items = parseElements( document.querySelectorAll( '.wp-block-image a' ) );

    options = {

      shareEl: false,
      // define gallery index (for URL)
      galleryUID: 1,

      getThumbBoundsFn: function(index) {

        // find thumbnail element
        let thumbnail = document.querySelectorAll( '.wp-block-image a img' )[index];

        // get window scroll Y
        let pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
        // optionally get horizontal scroll

        // get position of element relative to viewport
        let rect = thumbnail.getBoundingClientRect();

        // w = width
        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};

        // Good guide on how to get element coordinates:
        // http://javascript.info/tutorial/coordinates

      }
    };

    // PhotoSwipe opened from URL
    if(fromURL) {
      if(options.galleryPIDs) {
        // parse real index when custom PIDs are used 
        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
        for(let j = 0; j < items.length; j++) {
          if(items[j].pid == index) {
            options.index = j;
            break;
          }
        }
      } else {
        // in URL indexes start from 1
        options.index = parseInt(index, 10) - 1;
      }
    } else {
      options.index = parseInt(index, 10);
    }

    // exit if index not found
    if( isNaN(options.index) ) {
      return;
    }

    if(disableAnimation) {
        options.showAnimationDuration = 0;
    }

    // Pass data to PhotoSwipe and initialize it
    gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options );
    gallery.init();

};

// triggers when user clicks on thumbnail
let onThumbnailsClick = function( e ) {

    e = e || window.event;
    e.preventDefault ? e.preventDefault() : e.returnValue = false;

    let eTarget = e.target || e.srcElement;

    // find root element of slide
    let clickedListItem = closest(eTarget, function(el) {
      return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
    });

    if( !clickedListItem ) {
      return;
    }

    // find index of clicked item by looping through all child nodes
    // alternatively, you may define index via data- attribute
    let nodes = galleryElements,
        numNodes = galleryElements.length,
        nodeIndex = 0,
        index;

    for ( let i = 0; i < numNodes; i++ ) {

      if( nodes[i].nodeType !== 1 ) { 
        continue; 
      }

      if( nodes[i].parentNode === clickedListItem ) {
        index = nodeIndex;
        break;
      }

      nodeIndex++;
    
    }

    if( index >= 0 ) {
        // open PhotoSwipe if valid index found
        openPhotoSwipe( index, eTarget );
    }

    return false;

};

for( let i = 0, l = galleryElements.length; i < l; i++ ) {
 
    galleryElements[i].setAttribute( 'data-pswp-uid', i+1 );
    galleryElements[i].addEventListener( 'click', onThumbnailsClick );

}

// Parse URL and open gallery if it contains #&pid=3&gid=1
let hashData = photoswipeParseHash();

if( hashData.pid && hashData.gid ) {
    openPhotoSwipe( hashData.pid , galleryElements[ hashData.pid - 1 ], true, true );
}