<?php
/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Goldfinch_Lightbox
 * @subpackage Goldfinch_Lightbox/public
 * @author     Ink & Water Ltd <tom@inkandwater.co.uk>
 */

class Goldfinch_Lightbox_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_admin_scripts() {

        wp_register_script(
            'goldfinch-lightbox-blocks',
            plugin_dir_url( __FILE__ ) . 'assets/build/blocks.js',
            array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
            $this->version,
            false
        );

        wp_enqueue_script( 'goldfinch-lightbox-blocks' );

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        wp_register_script(
            'lightbox',
            plugin_dir_url( __FILE__ ) . 'assets/build/lightbox.min.js',
            array( 'jquery' ),
            $this->version,
            true
        );

        wp_register_script(
            'lightbox-media-text',
            plugin_dir_url( __FILE__ ) . 'assets/build/lightbox-media-text.min.js',
            array( 'jquery' ),
            $this->version,
            true
        );

        wp_register_script(
            'photoswipe',
            plugin_dir_url( __FILE__ ) . 'assets/build/photoswipe/photoswipe.min.js',
            array(),
            $this->version,
            true
        );

        wp_register_script(
            'photoswipe-ui',
            plugin_dir_url( __FILE__ ) . 'assets/build/photoswipe/photoswipe-ui-default.min.js',
            array(),
            $this->version,
            true
        );

        if ( has_block( 'core/image' ) ) {
            wp_enqueue_script( 'photoswipe' );
            wp_enqueue_script( 'photoswipe-ui' );
            wp_enqueue_script( 'lightbox' );
        }

        if ( has_block( 'core/media-text' ) ) {
            wp_enqueue_script( 'photoswipe' );
            wp_enqueue_script( 'photoswipe-ui' );
            wp_enqueue_script( 'lightbox-media-text' );
        }

    }

    /**
     * Register the CSS for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        wp_register_style(
            'photoswipe-styles',
            plugin_dir_url( __FILE__ ) . 'assets/build/css/photoswipe.min.css',
            array(),
            $this->version,
            'all'
        );

        if ( has_block( 'core/image' ) ) {
            wp_enqueue_style( 'photoswipe-styles' );
        }

        if ( has_block( 'core/media-text' ) ) {
            wp_enqueue_style( 'photoswipe-styles' );
        }

    }

    public function lightbox_template() {

        if ( has_block( 'core/image' ) || has_block( 'core/media-text' ) ) :

            add_action( 'goldfinch_footer_after',     'goldfinch_photoswipe_template',     10 );

        endif;

    }

}