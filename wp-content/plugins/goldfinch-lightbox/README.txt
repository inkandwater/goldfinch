=== Goldfinch Lightbox ===
Contributors: tomjnapier, paulrmyers
Donate link: https://inkandwater.co.uk
Tags:
Requires at least: 5.0.0
Tested up to: 5.2.2
Stable tag: 5.2.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds photoswipe lightbox functionality to images and buttons.

== Changelog ==

= 1.0.0 =
* Initial release.