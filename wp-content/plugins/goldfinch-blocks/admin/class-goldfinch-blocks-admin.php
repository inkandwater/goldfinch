<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/admin
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */

class Goldfinch_Blocks_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style(
            'editor_styles',
            plugins_url( 'public/build/build.editor.css', dirname( __FILE__ ) ),
            array( 'wp-edit-blocks' ),
            $this->version
        );

    }

    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script(
            'blocks',
            plugins_url( 'public/build/index.js', dirname( __FILE__ ) ),
            array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-block-editor' ),
            $this->version,
            true
        );

        $block_data = array(
            'apiKey' => get_option( 'goldfinch_map_api_key' ),
        );

        wp_localize_script(
            'blocks',
            'blockData',
            $block_data
        );

    }

}
