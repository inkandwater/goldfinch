const path           = require('path');
const webpack        = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CopyPlugin     = require('copy-webpack-plugin');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {

  entry: {
    'content-carousel': './src/blocks/content-carousel/content-carousel.init.js',
    'map': './src/blocks/map/map-init.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve( __dirname, "build/js" ),
    publicPath: "/build"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: {
            loader: 'vue-loader'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'css/[name].min.css',
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader?-url'
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000,
            name: '[name].[ext]'
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [

    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),

    // Move static files out of node_modules
    // (requiring some of these in js files seemed to be a minefield).
    new CopyPlugin([
      {
        from: 'node_modules/slick-carousel/slick/slick.min.js',
        to: './slick/',
        'toType' : 'dir'
      },
      {
        from: 'node_modules/slick-carousel/slick/ajax-loader.gif',
        to: './slick/img/[name].[ext]',
        toType: 'template'
      },
      {
        from: { glob: 'node_modules/slick-carousel/slick/fonts/*', dot: false },
        to: './slick/fonts/[name].[ext]',
        toType: 'template'
      }
    ]),

  ]

}