<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/public
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Goldfinch_Blocks_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style(
            'slick',
            plugins_url( 'public/build/css/slick.min.css', dirname( __FILE__ ) ),
            array(),
            $this->version
        );

        if ( has_block( 'goldfinch/content-carousel' ) ) {
            wp_enqueue_style( 'slick-styles' );
        }

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_register_script(
            'slick',
            plugin_dir_url( __DIR__ ) . 'public/build/slick/slick.min.js',
            array( 'jquery' ),
            $this->version,
            true
        );

        wp_register_script(
            'content-carousel',
            plugin_dir_url( __DIR__ ) . 'public/build/content-carousel.min.js',
            array( 'jquery', 'slick' ),
            $this->version,
            true
        );

        $apikey = get_option( 'goldfinch_map_api_key' );

        if ( has_block( 'goldfinch/map' ) ) {

            wp_enqueue_script(
                'map-js',
                plugin_dir_url( __DIR__ ) . 'public/build/map.min.js',
                array( 'jquery' ),
                $this->version,
                true
            );

            wp_enqueue_script(
                'maps-api',
                "https://maps.googleapis.com/maps/api/js?key={$apikey}#asyncload",
                array(),
                '',
                true
            );

            $data = array(
                'siteUrl' => get_site_url(),
                'apiKey' => $apikey,
            );

            wp_localize_script(
                'map-js',
                'blockData',
                $data
            );

        }

        if ( has_block( 'goldfinch/content-carousel' ) ) {
            wp_enqueue_script( 'slick' );
            wp_enqueue_script( 'content-carousel' );
        }

    }

    public function custom_meta() {

        register_post_meta( 'page', '_gf_header_theme_light', array(
            'show_in_rest'      => true,
            'single'            => true,
            'type'              => 'boolean',
            'auth_callback'     => function() {
                return current_user_can( 'edit_posts' );
            }
        ) );

    }

}
