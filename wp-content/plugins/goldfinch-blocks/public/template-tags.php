<?php
if ( ! function_exists( 'get_button_block_icon' ) ) :

    function get_button_block_icon( $icon = '' ) {

        $icons = array(
            'ArrowRight' => array(
                'name' => 'Arrow Right',
                'icon' => '#icon--arrow-right',
            ),
            'ArrowDown' => array(
                'name' => 'Arrow Down',
                'icon' => '#icon--arrow-down',
            ),
            'Download' => array(
                'name' => 'Download',
                'icon' => '#icon--download',
            ),
            'ExternalLink' => array(
                'name' => 'External Link',
                'icon' => '#icon--arrow-up-right',
            ),
        );

        if ( ! empty( $icon ) && isset( $icons[ $icon ] ) ) {
            return $icons[ $icon ]['icon'];
        }

        return $icons;
    }

endif;

if ( ! function_exists( 'button_block_icon_filter' ) ) :

    function button_block_icon_filter( $block_content, $block ) {

        if ( "core/button" !== $block['blockName'] ) {
            return $block_content;
        }

        if ( isset( $block['attrs']['selectedIcon'] ) && $block['attrs']['selectedIcon'] != 'None' ) :

            $selectedIcon = $block['attrs']['selectedIcon'];

            $icon = get_button_block_icon( $selectedIcon );
            $to_insert = sprintf( '<svg viewBox="0 0 20 20" class="wp-block-button__link-icon" role="img" focusable="false" aria-hidden="true"><use href="%s"></use></svg></a>', $icon );

            $block_content = preg_replace( '/<\/a>/', $to_insert, $block_content );

        endif;

        return $block_content;

    }

endif;

if ( ! function_exists( 'file_block_icon_filter' ) ) :

    function file_block_icon_filter( $block_content, $block ) {

        if ( "core/file" !== $block['blockName'] ) {
            return $block_content;
        }

        $to_insert = sprintf( '<svg class="wp-block-file__icon" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" role="img" focusable="false" aria-hidden="true"><g fill="none"><path d="M19 13v4a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-4M5 8l5 5 5-5M10 13V1"/></g></svg></a>', esc_attr( "icon--download" ) );
        $block_content = preg_replace( '/<\/a>/', $to_insert, $block_content );

        return $block_content;

    }

endif;

if ( ! function_exists( 'goldfinch_content_carousel' ) ) :

    function goldfinch_content_carousel( $attributes ) {

        $style = ( array_key_exists( 'className', $attributes ) ) ? $attributes['className'] : 'is-style-default';

        $class = array( 'wp-block-goldfinch-content-carousel', $style );
        $blockClass = 'wp-block-goldfinch-content-carousel';

        $align = $attributes['align'] ?? null;

        if ( $align ) {
            $class[] .= "align{$align}";
        } else {
            $class[] .= null;
        }

        $no_of_child_blocks = count($attributes['childBlocks']);

        if ( 0 != $no_of_child_blocks ) {
            $class[] .= "slides-{$no_of_child_blocks}";
        }

        if ( array_key_exists( 'className', $attributes ) ) : ?>

            <div class="<?php echo implode( " ", $class ); ?>">

                <div class="<?php echo $blockClass . '__slides'; ?>">
                    <?php foreach ( $attributes['childBlocks'] as $index => $slide ) :

                        if ( $slide['name'] == "goldfinch/slide" ) {

                            $imageUrl = $slide['attributes']['image'][0]['attributes']['url']; ?>

                            <figure class="<?php echo $blockClass . '__slide'; ?>" style="background-image: url(<?php echo esc_url( $imageUrl ); ?>);">
                            </figure>

                        <?php } else if ( ( $slide['name'] == "goldfinch/page-link" ) ) {

                            $image = wp_get_attachment_image_url( $slide['attributes']['image'], 'full' ); ?>

                            <figure class="<?php echo $blockClass . '__slide'; ?>" style="background-image: url(<?php echo esc_url( $image ); ?>);">
                                <a href="<?php echo esc_url($slide['attributes']['pageLink'] ); ?>" rel="page link"><span class="screen-reader-text"><?php echo __( "Link to ", "goldfinch" ) . esc_html( $slide['attributes']['pageTitle'] ); ?></span></a>
                            </figure>

                        <?php } ?>

                    <?php endforeach ?>

                </div>

                <nav class="<?php echo $blockClass . '__slides-navigation'; ?>">
                    <?php foreach ( $attributes['childBlocks'] as $index => $slide ) :

                        if ( $slide['name'] == "goldfinch/slide" ) {

                            $imageID = $slide['attributes']['image'][0]['attributes']['id'];
                            $image = wp_get_attachment_image( $imageID, 'square' ); ?>

                            <article class="<?php echo $blockClass . '__navigation-slide'; ?>">

                                <figure class="<?php echo $blockClass . '__slide-thumbnail mb-3'; ?>">
                                    <?php echo $image; ?>
                                </figure>

                                <h2 class="<?php echo $blockClass . '__navigation-slide-heading heading--l'; ?>"><?php echo esc_html( $slide['attributes']['heading'] ); ?></h2>
                                <p class="<?php echo $blockClass . '__navigation-slide-content'; ?>"><?php echo esc_html( $slide['attributes']['content'] ); ?></p>

                            </article>

                        <?php } else if ( ( $slide['name'] == "goldfinch/page-link" ) ) {

                            $imageID = ( $slide['name'] == "goldfinch/slide" ) ? attachment_url_to_postid( $slide['attributes']['image'][0]['attributes']['url'] ) : $slide['attributes']['image'];
                            $image = wp_get_attachment_image( $imageID, 'square' ); ?>

                            <article class="<?php echo $blockClass . '__navigation-slide'; ?>">

                                    <figure class="<?php echo $blockClass . '__slide-thumbnail mb-3'; ?>">
                                        <?php echo $image; ?>
                                    </figure>

                                    <h2 class="<?php echo $blockClass . '__navigation-slide-heading heading--l'; ?>"><a href="<?php echo esc_url($slide['attributes']['pageLink'] ); ?>" rel="page link"><?php echo esc_html( $slide['attributes']['pageTitle'] ); ?></a></h2>
                                    <p class="<?php echo $blockClass . '__navigation-slide-content'; ?>"><?php echo esc_html( $slide['attributes']['pageExcerpt'] ); ?></p>

                            </article>

                        <?php } ?>

                    <?php endforeach ?>
                </nav>

            </div>

        <?php else : ?>
            <div class="<?php echo implode( " ", $class ); ?>">

                <?php foreach ( $attributes['childBlocks'] as $index => $slide ) :

                    if ( $slide['name'] == "goldfinch/slide" ) {

                        $imageID = $slide['attributes']['image'][0]['attributes']['id'];
                        $image = wp_get_attachment_image( $imageID, 'slide' ); ?>

                         <article class="<?php echo $blockClass . '__slide'; ?>">

                            <figure class="<?php echo $blockClass . '__slide-thumbnail mb-2'; ?>">
                                <?php echo $image; ?>
                            </figure>

                            <div class="<?php echo $blockClass . '__slide-body'; ?>">
                                <h2 class="<?php echo $blockClass . '__navigation-slide-heading heading--l'; ?>"><?php echo esc_html( $slide['attributes']['heading'] ); ?></h2>
                                <p class="<?php echo $blockClass . '__navigation-slide-content'; ?>"><?php echo esc_html( $slide['attributes']['content'] ); ?></p>
                            </div>
                        </article>

                    <?php } else if ( ( $slide['name'] == "goldfinch/page-link" ) ) {

                        $imageID = ( $slide['name'] == "goldfinch/slide" ) ? attachment_url_to_postid( $slide['attributes']['image'][0]['attributes']['url'] ) : $slide['attributes']['image'];
                        $image = wp_get_attachment_image( $imageID, 'slide' ); ?>

                        <article class="<?php echo $blockClass . '__slide'; ?>">

                            <figure class="<?php echo $blockClass . '__slide-thumbnail mb-2'; ?>">
                                <?php echo $image; ?>
                            </figure>

                            <div class="<?php echo $blockClass . '__slide-body'; ?>">
                                <h2 class="<?php echo $blockClass . '__navigation-slide-heading heading--l'; ?>"><a href="<?php echo esc_url($slide['attributes']['pageLink'] ); ?>" rel="page link"><?php echo esc_html( $slide['attributes']['pageTitle'] ); ?></a></h2>
                                <p class="<?php echo $blockClass . '__navigation-slide-content'; ?>"><?php echo esc_html( $slide['attributes']['pageExcerpt'] ); ?></p>
                            </div>
                        </article>

                    <?php } ?>

                <?php endforeach ?>

            </div>
        <?php endif;

    }

endif;

if ( ! function_exists( 'goldfinch_faqs' ) ) :

    function goldfinch_faqs( $attributes ) {

        $class = array( 'wp-block-goldfinch-faqs' );
        $blockClass = 'wp-block-goldfinch-faqs';

        $align = $attributes['align'] ?? null;

        if ( $align ) {
            $class[] .= "align{$align}";
        } else {
            $class[] .= null;
        }

        $no_of_child_blocks = count($attributes['childBlocks']);

        if ( 0 != $no_of_child_blocks ) {
            $class[] .= "questions-{$no_of_child_blocks}";
        } ?>

        <div class="<?php echo implode( " ", $class ); ?>">

            <ol class="<?php echo $blockClass . '__questions'; ?>">

                <?php foreach ( $attributes['childBlocks'] as $index => $question ) {

                    $slug = strtolower( str_replace(" ", "-", $question['attributes']['title'] ) ); ?>

                    <li><a href="<?php echo '#' . $slug; ?>" class="<?php echo $blockClass . '__question'; ?>"><?php echo esc_html( $question['attributes']['title'] ); ?></a></li>

                <?php } ?>

                </ol>

            <dl class="<?php echo $blockClass . '__content'; ?>">

                <?php foreach ( $attributes['childBlocks'] as $index => $question ) {

                    $slug = strtolower( str_replace(" ", "-", $question['attributes']['title'] ) ); ?>

                    <dt id="<?php echo $slug; ?>" class="<?php echo $blockClass . '__question-title h3'; ?>"><?php echo esc_html( $question['attributes']['title'] ); ?></dt>
                    <dd class="<?php echo $blockClass . '__question-content'; ?>"><?php echo wpautop( wp_kses_post( $question['attributes']['content'] ) ); ?></dd>

                <?php } ?>

            </dl>

        </div>

    <?php }

endif;