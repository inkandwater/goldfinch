<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
function goldfinch_blocks_register_settings() {

    add_option( 'goldfinch_map_api_key', '' );
    register_setting( 'goldfinch_blocks_options_group', 'goldfinch_map_api_key' );

 }

function goldfinch_blocks_register_options_page() {

    add_options_page(
        'Goldfinch Blocks Settings',
        'Custom Block Settings',
        'manage_options',
        'goldfinch-settings',
        'goldfinch_blocks_register_content'
    );

}

function goldfinch_blocks_register_content() { ?>

    <div>
        <h1><?php echo __( 'Goldfinch Blocks Settings', 'goldfinch' ); ?></h1>
        <form method="post" action="options.php">
            <?php settings_fields( 'goldfinch_blocks_options_group' ); ?>
            <br>
            <h3><?php echo __( 'Map API Key', 'goldfinch' ); ?></h3>
            <p><?php echo __( 'To get a unique API Key, you need to set up the credentials on the <a target="_blank" href="https://console.cloud.google.com/">Google API Console</a>.', 'goldfinch' ); ?></p>
            <table>
                <tr valign="top">
                    <th scope="row"><label for="goldfinch_map_api_key"><?php echo __( 'API Key:', 'goldfinch' ); ?> </label></th>
                    <td><input
                        type  = "text"
                        id    = "goldfinch_map_api_key"
                        name  = "goldfinch_map_api_key"
                        value = "<?php echo get_option('goldfinch_map_api_key'); ?>" /></td>
                </tr>
            </table>
            <br>
            <?php  submit_button(); ?>
        </form>
    </div>

<?php }

function goldfinch_blocks_link( $links ) {


    $url = esc_url( add_query_arg(
        'page',
        'goldfinch-settings',
        get_admin_url() . 'admin.php'
    ) );

    // Create the link.
    $settings_link = "<a href='$url'>" . __( 'Settings' ) . '</a>';

    // Adds the link to the end of the array.
    array_push(
        $links,
        $settings_link
    );
    return $links;

}

add_action( 'admin_init', 'goldfinch_blocks_register_settings' );
add_action( 'admin_menu', 'goldfinch_blocks_register_options_page');

add_filter( 'plugin_action_links_goldfinch/plugin.php', 'goldfinch_blocks_link' );