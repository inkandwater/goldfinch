<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '/src/blocks/content-carousel/index.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '/src/blocks/faqs/index.php';

add_filter( 'block_categories_all' , function( $categories ) {

    $categories[] = array(
        'slug'  => 'custom-goldfinch-category',
        'title' => 'Goldfinch'
    );

    return $categories;

} );