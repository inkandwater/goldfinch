const { createBlock } = wp.blocks;

const transforms = {
     from: [
         {
            type: 'block',
            blocks: [ 'goldfinch/page-link' ],
            transform: () => {
                return createBlock( 'goldfinch/slide' );
            },
         }
     ],
     to: [
        {
            type: 'block',
            blocks: [ 'goldfinch/page-link' ],
            transform: () => {
                return createBlock( 'goldfinch/page-link' );
            },
         }
     ],
 };

export default transforms;