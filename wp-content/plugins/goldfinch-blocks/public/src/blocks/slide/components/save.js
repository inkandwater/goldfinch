const { getBlockDefaultClassName } = wp.blocks;
const { RichText, InnerBlocks, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { heading, content } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/slide' );
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>

            <div className={ blockClass + "__thumbnail" }>
                <InnerBlocks.Content
                    template = { [['core/image']] }
                />
            </div>

            <RichText.Content
                className={ blockClass + '__heading' }
                tagName="h4"
                value={ heading }
            />

            <RichText.Content
                className={ blockClass + '__content' }
                tagName="p"
                value={ content }
            />

        </div>
    );
});

export default save