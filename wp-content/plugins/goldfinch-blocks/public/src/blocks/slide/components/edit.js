const { __ } = wp.i18n;
const { withSelect } = wp.data;
const { getBlockDefaultClassName } = wp.blocks;
const { RichText, InnerBlocks, useBlockProps } = wp.blockEditor;

const ALLOWED_BLOCKS = [ 'goldfinch/slide' ];

const edit = ( ({ innerBlocks, attributes, setAttributes, className } ) => {

    const { heading, content, image } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/slide' );
    const blockProps = useBlockProps();

    setAttributes({ image: innerBlocks });

    return (
        <div { ...blockProps }>

            <div className={ blockClass + "__thumbnail" }>
                <InnerBlocks
                    template      = { [['core/image']] }
                    templateLock  = { 'all' }
                    allowedBlocks = { ALLOWED_BLOCKS }
                />
            </div>

            <RichText
                className={ blockClass + '__heading' }
                tagName="h4"
                value={ heading }
                onChange={ ( heading ) => setAttributes( { heading } ) }
                placeholder={ __( 'Heading...' ) }
            />

            <RichText
                className={ blockClass + '__content' }
                tagName="p"
                value={ content }
                onChange={ ( content ) => setAttributes( { content } ) }
                placeholder={ __( 'Content...' ) }
            />

        </div>
    );

});

export default withSelect( ( select, blockData ) => {

    return { innerBlocks: select( 'core/block-editor' ).getBlocks( blockData.clientId ) };

} )( edit )