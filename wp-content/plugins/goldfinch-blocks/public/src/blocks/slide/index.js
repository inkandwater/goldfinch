const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import metadata from './block.json';
import edit from './components/edit';
import save from './components/save';
import transforms from './components/transforms';

registerBlockType( metadata, {

    title: __( 'Slide' ),
    parent: [ "goldfinch/content-carousel" ],
    edit,
    save,
    transforms

});