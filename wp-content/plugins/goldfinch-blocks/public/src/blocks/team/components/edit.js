const { __ }                = wp.i18n;
const { Fragment } = wp.element;
const { RichText, BlockControls, MediaPlaceholder, MediaUploadCheck, MediaUpload, useBlockProps } = wp.blockEditor;
const { ToolbarGroup, Button } = wp.components;

const edit = ( ({ attributes, className, setAttributes }) => {

    const { imageID, url, name, role, bio, alt } = attributes;
    const blockProps = useBlockProps();
    const blockClass = "team-member"

    const onSelectMedia = media => {

        if ( ! media || ! media.url ) {
            return;
        }

        let imageSrc = '';
        let altText = media.alt;

        if( '' == media.alt ) {
            altText = media.title
        }

        setAttributes( {
            imageID: media.id,
            url: media.sizes.square.url,
            alt: altText
        } );

    }

    const imageControls = (
        <Fragment>
             { ! imageID &&
                <MediaPlaceholder
                    icon   = "format-gallery"
                    labels = { {
                        title: __( 'Featured Image' ),
                        instructions: __( 'Drag images, upload new ones or select files from your library.' ),
                    } }
                    onSelect = { onSelectMedia }
                    accept   = "image/*"
                />
            }
            { !! imageID &&
                <BlockControls>
                    <MediaUploadCheck>
                    <ToolbarGroup>
                        <MediaUpload
                            onSelect = { onSelectMedia }
                            value    = { imageID }
                            render   = { ( { open } ) => (
                                <Button
                                    className="components-toolbar__control"
                                    label={ __( 'Edit media' ) }
                                    icon="edit"
                                    onClick={ open }
                                />
                        ) } />
                    </ToolbarGroup>
                    </MediaUploadCheck>
                </BlockControls>
            }
        </Fragment>
    );

    return (
        <article { ...blockProps }>

            { imageControls }

            { !! imageID &&
                <Fragment>
                    <img className={ blockClass + '__image' } src={ url } />
                </Fragment>
            }

            <RichText
                className   = { blockClass + "__name" }
                placeholder = { __( "Team name" ) }
                tagName     = "h3"
                value       = { name }
                onChange    = { ( value ) => setAttributes( { name: value } ) }
            />
            <RichText
                className   = { blockClass + "__role" }
                placeholder = { __( "Add a role" ) }
                tagName     = "span"
                value       = { role }
                onChange    = { ( value ) => setAttributes( { role: value } ) }
            />
            <RichText
                className   = { blockClass + "__bio" }
                placeholder = { __( "Add a bio" ) }
                tagName     = "p"
                value       = { bio }
                onChange    = { ( value ) => setAttributes( { bio: value } ) }
            />

        </article>
    )

});

export default edit