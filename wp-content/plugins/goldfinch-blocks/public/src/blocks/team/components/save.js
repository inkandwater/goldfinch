const { RichText, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { imageID, url, name, role, bio, alt } = attributes;
    const blockProps = useBlockProps.save();
    const blockClass = "team-member"

    return (
        <article { ...blockProps }>

            { !! imageID &&
                <figure className={ blockClass + "__image" }>
                    <img  src={ url } alt={ alt } />
                </figure>
            }

            <div className={ blockClass + "__detail" }>

              <RichText.Content
                  className   = { blockClass + "__name" }
                  tagName     = "h3"
                  value       = { name }
              />

              { !! role &&
                <RichText.Content
                    className   = { blockClass + "__role" }
                    tagName     = "span"
                    value       = { role }
                />
              }

              { !! bio &&
                  <RichText.Content
                      className   = { blockClass + "__bio text--small" }
                      tagName     = "p"
                      value       = { bio }
                  />
              }

            </div>

        </article>
    )

});

export default save