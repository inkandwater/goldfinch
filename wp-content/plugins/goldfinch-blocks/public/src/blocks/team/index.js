const { registerBlockType } = wp.blocks;

import metadata from './block.json';
import edit from './components/edit';
import save from './components/save';

import './editor.scss';

registerBlockType( metadata, {

    icon: 'admin-users',
    edit,
    save

});