const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import metadata from './block.json';
import edit from './components/edit';
import save from './components/save';

registerBlockType( metadata, {

    title: __( "Question" ),
    parent: [ "goldfinch/faqs" ],
    edit,
    save

});