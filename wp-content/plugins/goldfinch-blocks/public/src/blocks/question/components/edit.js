const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { RichText, useBlockProps } = wp.blockEditor;

const edit = ( ({ attributes, setAttributes, className } ) => {

    const { title, content } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/question' );
    const blockProps = useBlockProps();

    return (
        <div { ...blockProps }>

            <RichText
                className={ blockClass + '__title' }
                tagName="h4"
                value={ title }
                onChange={ ( value ) => setAttributes( { title: value } ) }
                placeholder={ __( 'Heading...' ) }
            />

            <RichText
                className={ blockClass + '__content' }
                tagName="p"
                value={ content }
                onChange={ ( value ) => setAttributes( { content: value } ) }
                placeholder={ __( 'Content...' ) }
            />

        </div>
    );

})

export default edit