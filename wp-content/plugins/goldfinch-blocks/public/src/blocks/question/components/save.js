const { getBlockDefaultClassName } = wp.blocks;
const { RichText, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { title, content } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/question' );
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>

            <RichText.Content
                className={ blockClass + '__title' }
                tagName="h4"
                value={ title }
            />

            <RichText.Content
                className={ blockClass + '__content' }
                tagName="p"
                value={ content }
            />

        </div>
    );
});

export default save