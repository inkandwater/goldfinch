const { __ }                = wp.i18n;
const { Fragment }          = wp.element;
const { getBlockDefaultClassName } = wp.blocks;
const { withSelect } = wp.data;
const { RichText, MediaUpload, MediaUploadCheck, BlockControls, MediaPlaceholder, InspectorControls, useBlockProps } = wp.blockEditor;
const { ToolbarGroup, Button, Panel, PanelBody } = wp.components;

import Select from 'react-select';

const edit = ( ({ pages, className, setAttributes, attributes }) => {

    if ( ! pages ) {
        return "Loading...";
    }

    const { page, postType, pageTitle, pageLink, pageExcerpt, image, imageAlt, imageUrl, subTitle } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/page-link' );
    const blockProps = useBlockProps();

    const pageOptions = pages.map( post => {
        return ( { label: post.title.rendered, value: post } );
    } );

    const onSelectMedia = ( media ) => {

        if ( ! media || ! media.url ) {
            setAttributes( { url: undefined, id: undefined } );
            return;
        }

        setAttributes( {
            imageUrl: media.sizes.square.url,
            image: media.id,
            imageAlt: media.alt
        } );

    };

    const onChangePage = value => {
        setAttributes({
            page:        value,
            pageTitle:   value.value.title.rendered,
            pageLink:    value.value.link,
            pageExcerpt: value.value.excerpt.raw
            });
    }

    const imageControls = (
        <Fragment>
                { ! image && !! page &&
                <MediaPlaceholder
                    icon   = "format-gallery"
                    labels = { {
                        title: __( 'Featured Image' ),
                        instructions: __( 'Drag images, upload new ones or select files from your library.' ),
                    } }
                    onSelect = { onSelectMedia }
                    accept   = "image/*"
                />
            }
            { !! image &&
                <BlockControls>
                    <MediaUploadCheck>
                    <ToolbarGroup>
                        <MediaUpload
                            onSelect = { onSelectMedia }
                            value    = { image }
                            render   = { ( { open } ) => (
                                <Button
                                    className="components-toolbar__control"
                                    label={ __( 'Edit media' ) }
                                    icon="edit"
                                    onClick={ open }
                                />
                        ) } />
                    </ToolbarGroup>
                    </MediaUploadCheck>
                </BlockControls>
            }
        </Fragment>
    );

    const pageSelectControls = (
        <InspectorControls>
            <Panel>
                { !! page &&
                    <PanelBody
                        title={ __( 'Select a Page' ) }
                        initialOpen={ true }>
                        <Select
                            value    = { page }
                            options  = { pageOptions }
                            onChange = { onChangePage }
                            placeholder = { __('Select...') }
                            isSearchable
                        />
                    </PanelBody>
                }
            </Panel>
        </InspectorControls>
    );

    return (
        <article { ...blockProps }>

            { ! page &&
                <Select
                    value    = { page }
                    options  = { pageOptions }
                    onChange = { onChangePage }
                    placeholder = { __('Select...') }
                    isSearchable
                />
            }

            { pageSelectControls }
            { imageControls }

            { !! page && image &&
                <figure class={ blockClass + "__thumbnail" }>
                    <img src={ imageUrl } alt={ imageAlt } />
                </figure>
            }

            { !! page &&
                <div class={ blockClass + "__body" }>

                    <RichText
                        className   = { blockClass + "__subtitle subheading" }
                        tagName     = "span"
                        value       = { subTitle }
                        placeholder = { __( "Sub Title" ) }
                        onChange={ ( value ) =>
                            setAttributes( { subTitle: value } )
                        }
                    />

                    <RichText
                        className   = { blockClass + "__title heading--l" }
                        tagName     = "h3"
                        value       = { pageTitle }
                        placeholder = { __( "Link Title" ) }
                        onChange={ ( value ) =>
                            setAttributes( { pageTitle: value } )
                        }
                    />

                    <RichText
                        className   = { blockClass + "__excerpt" }
                        tagName     = "p"
                        value       = { pageExcerpt }
                        placeholder = { __( "Add a short description" ) }
                        onChange={ ( newExcerpt ) =>
                            setAttributes( { pageExcerpt: newExcerpt } )
                        }
                    />
                </div>
            }

        </article>
    );

})

export default withSelect( ( select ) => {

    const { getEntityRecords } = select( 'core' );
    const postId = select( 'core/editor' ).getCurrentPostId();

    return {
        pages: getEntityRecords( 'postType', 'page', {
            orderby:  'title',
            order:    'asc',
            per_page: -1,
            exclude: postId
        } )
    };

} )( edit )