const { Fragment }          = wp.element;
const { RichText, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { page, pageTitle, pageLink, pageExcerpt, image, imageUrl, subTitle, imageAlt } = attributes;
    const blockClass = 'page-link';
    const blockProps = useBlockProps.save();

    return (
        <article { ...blockProps }>

            { !! image &&
                <figure className={ blockClass + "__thumbnail" }>
                    <img src={ imageUrl } alt={ imageAlt } />
                </figure>
            }

            <div class={ blockClass + "__body" }>

                { !! page &&
                    <Fragment>
                        <span className={ blockClass + "__subtitle subheading" }>{ subTitle }</span>
                        <h3 className={ blockClass + "__title heading--l" }><a class={ "text-decoration--none" } href={ pageLink }>{ pageTitle }</a></h3>
                    </Fragment>
                }
                { !! pageExcerpt &&
                    <RichText.Content
                        className   = { blockClass + "__excerpt" }
                        tagName     = "p"
                        value       = { pageExcerpt }
                    />
                }

            </div>

        </article>
    )

})

export default save