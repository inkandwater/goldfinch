const { InnerBlocks, useBlockProps } = wp.blockEditor;

const ALLOWED_BLOCKS = [ 'goldfinch/page-link' ];

const edit = ( ({ attributes, className }) => {

    const { align } = attributes;
    const blockProps = useBlockProps();

    return (
        <div { ...blockProps }>
            <InnerBlocks
                template      = {[ ['goldfinch/page-link'] ]}
                allowedBlocks = { ALLOWED_BLOCKS }
            />
        </div>
    );

  })

  export default edit