const { InnerBlocks, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes }) => {

    const { align, className } = attributes;
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>
            <InnerBlocks.Content />
        </div>
    );

})

export default save