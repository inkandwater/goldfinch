const { addFilter } = wp.hooks;

//restrict to specific block names
const allowedBlocks = [ 'core/file' ];
import classNames from 'classnames';

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass( extraProps, blockType, attributes ) {

  const { showDownloadButton } = attributes;

  //check if attribute exists for old Gutenberg version compatibility
  //add class only when hasNavIcon = false
  //add allowedBlocks restriction
  if ( showDownloadButton === true && allowedBlocks.includes( blockType.name ) ) {
    extraProps.className = classNames( extraProps.className, 'has-download-button' );
  }

  return extraProps;

}

addFilter(
  'blocks.getSaveContent.extraProps',
  'bipc/applyExtraClass',
  applyExtraClass
);