const { InnerBlocks, useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { childBlocks, align } = attributes;
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>
            <InnerBlocks.Content
                template = { [['goldfinch/slide']] }
            />
        </div>
    );

})

export default save