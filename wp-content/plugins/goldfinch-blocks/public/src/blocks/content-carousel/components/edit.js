const { withSelect } = wp.data;
const { InnerBlocks, useBlockProps } = wp.blockEditor;

const ALLOWED_BLOCKS = [ 'goldfinch/slide', 'goldfinch/page-link' ];

const edit = ( ({ innerBlocks, className, setAttributes, attributes }) => {

    const { childBlocks, align } = attributes;
    const blockProps = useBlockProps();
    setAttributes({ childBlocks: innerBlocks });

    return (
        <div { ...blockProps }>
            <InnerBlocks
                template      = { [['goldfinch/slide']] }
                allowedBlocks = { ALLOWED_BLOCKS }
            />
        </div>
    );

});

export default withSelect( ( select, blockData ) => {

    return { innerBlocks: select( 'core/block-editor' ).getBlocks( blockData.clientId ) };

})( edit )