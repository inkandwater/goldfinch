import ContentCarousel from "./content-carousel.js";

( function( $ ) {

  $( document ).ready( function() {

  	let carousel = new ContentCarousel();

    carousel.init();

  });

})( jQuery );