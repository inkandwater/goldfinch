import './slick.scss';

export default class ContentCarousel {

  constructor() {

    this.defaultSettings = {

        infinite: false,
        arrows: false,
        dots: true,
        fade: false,
        draggable: true,
        swipe: true,
        swipeToSlide: true,
        touchMove: true,
        slide: '.wp-block-goldfinch-content-carousel__slide',
        mobileFirst : true,
        centerMode: true,
        centerPadding: 20,
        responsive: [
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 1
            }
          },
          {
            breakpoint: 0,
            settings: {
              slidesToShow: 1.1
            }
          }
        ]
    }

    this.sliderSettings = {

        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        dots: true,
        fade: false,
        swipe: true,
        swipeToSlide: true,
        touchMove: true,
        asNavFor: '.wp-block-goldfinch-content-carousel__slides-navigation',
    }

    this.sliderNavSettings = {

        asNavFor: '.wp-block-goldfinch-content-carousel__slides',
        focusOnSelect: true,
        infinite: false,
        arrows: false,
        slidesToScroll: 1,
        mobileFirst : true,
        responsive: [
          {
            breakpoint: 1100,
            settings: {
              slidesToShow: 4
            }
          },
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 3.2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2.2
            }
          },
          {
            breakpoint: 0,
            settings: {
              slidesToShow: 1.2
            }
          }
        ]

    }

  }

  init() {

    jQuery( '.is-style-default.wp-block-goldfinch-content-carousel' ).slick( this.defaultSettings );
    jQuery( '.is-style-with-navigation .wp-block-goldfinch-content-carousel__slides' ).slick( this.sliderSettings );
    jQuery( '.is-style-with-navigation .wp-block-goldfinch-content-carousel__slides-navigation' ).not('.slick-initialized').slick( this.sliderNavSettings );

  }

}