<?php
/**
 * Renders the `goldfinch/content-carousel` block on server.
 *
 * @param array $attributes The block attributes.
 *
 * @return string Returns the post content with latest posts added.
 */
function render_content_carousel_block( $attributes ) {

    $block_content = '';

    ob_start();

    goldfinch_content_carousel( $attributes );

    $block_content = ob_get_contents();
    ob_end_clean();

    return $block_content;

}

/**
 * Registers the `goldfinch/content-carousel` block on server.
 */
function register_content_carousel_block() {

    register_block_type(
        'goldfinch/content-carousel',
        array(
            'attributes'      => array(
                'align' => array(
                    'type' => 'string',
                    'default' => 'wide'
                ),
                'childBlocks' => array(
                    'type' => 'array'
                ),
                'blockClasses' => array(
                    'type' => 'string'
                ),
            ),
            'render_callback' => 'render_content_carousel_block',
        )
    );

}

add_action( 'init', 'register_content_carousel_block' );