const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import "./editor.scss";
import metadata from './block.json';
import edit from './components/edit';
import save from './components/save';

registerBlockType( metadata, {

    title: __( 'Content Carousel' ),
    category: "custom-goldfinch-category",
    icon: <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#cf4830" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3.01" y2="6"></line><line x1="3" y1="12" x2="3.01" y2="12"></line><line x1="3" y1="18" x2="3.01" y2="18"></line></svg>,
    styles: [
        {
            name: 'default',
            label: 'Default',
            isDefault: true
        },
        {
            name: 'with-navigation',
            label: 'With Navigation',
        }
    ],
    edit,
    save

} );