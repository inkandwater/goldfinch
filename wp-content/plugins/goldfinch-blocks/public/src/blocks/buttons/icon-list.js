const iconList = {
    None: {
        name: 'None',
        slug: ''
    },
    ArrowRight: {
        name: 'Arrow Right',
        slug: 'arrow-right'
    },
    ArrowDown: {
        name: 'Arrow Down',
        slug: 'arrow-down'
    },
    Download: {
        name: 'Download',
        slug: 'download'
    },
    ExternalLink: {
        name: 'External Link',
        slug: 'external-link'
    }
}

export default iconList;

export const getIconName = ( icon ) => {
	return iconList[ icon ].name;
};

export const getIconSlug = ( icon ) => {
	return iconList[ icon ].slug;
};