import classNames from 'classnames';
import Select from 'react-select';
import './editor.scss';

const { __ } = wp.i18n;
const { addFilter } = wp.hooks;
const { getBlockDefaultClassName } = wp.blocks;
const { Fragment }  = wp.element;
const { InspectorControls } = wp.blockEditor;
const { createHigherOrderComponent } = wp.compose;
const { PanelBody } = wp.components;

// Authored Parameters
import { getIconName, getIconSlug } from './icon-list';
import iconList from './icon-list';

//restrict to specific block names
const allowedBlocks = [ 'core/button' ];

/**
 * Add custom attribute for mobile visibility.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
function addAttributes( settings ) {

  //check if object exists for old Gutenberg version compatibility
  //add allowedBlocks restriction
  if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){

    settings.attributes = Object.assign( settings.attributes, {
      selectedIcon: {
        type: 'string',
        default: ''
      },
      selectedOption: {
          type: 'object'
      },
      borderRadius: {
          type: 'number',
          default: 3
      }
    });

  }

  return settings;
}

/**
 * Add mobile visibility controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
  return ( props ) => {

    const {
      name,
      attributes,
      setAttributes,
      isSelected,
    } = props;

    const { selectedIcon, selectedOption } = attributes;

    const iconOptions = Object.keys( iconList ).map( icon => {
        return ({ label: getIconName( icon ), value: getIconName( icon ).replace(/\s/g,'') })
    });

    return (
      <Fragment>
        <BlockEdit {...props} />

        { isSelected && allowedBlocks.includes( name ) &&
          <InspectorControls>
            <PanelBody
                title={ __( 'Icon' ) }
                initialOpen={ true }>
                <Select
                    label    = { __( 'Icon' ) }
                    className= { "select-wide" }
                    value    = { selectedOption }
                    options  = { iconOptions }
                    onChange = { option => {
                        setAttributes( { selectedIcon: option.value } )
                        setAttributes( { selectedOption: option } )
                    }}
                    placeholder = { __( 'Choose Icon' ) }
                    help={ !! selectedIcon ? __( 'Indicate that this button is navigational.' ) : __( 'Indicate that this button is navigational.' ) }
                />
            </PanelBody>
          </InspectorControls>
        }

      </Fragment>
    );
  };
}, 'withAdvancedControls');

/**
 * Add custom element class in save element.
 *
 * @param {Object} extraProps     Block element.
 * @param {Object} blockType      Blocks object.
 * @param {Object} attributes     Blocks attributes.
 *
 * @return {Object} extraProps Modified block element.
 */
function applyExtraClass( extraProps, blockType, attributes ) {

    const { selectedIcon, selectedOption } = attributes;

    const blockClass = getBlockDefaultClassName( 'core/button' );

    //check if attribute exists for old Gutenberg version compatibility
    //add class only when hasNavIcon = false
    //add allowedBlocks restriction
    if ( typeof selectedIcon !== 'undefined' && selectedIcon && allowedBlocks.includes( blockType.name ) ) {
      extraProps.className = classNames( extraProps.className, blockClass + "--" + getIconSlug( selectedIcon ) );
    }

    return extraProps;

}

const withClassName = createHigherOrderComponent( ( BlockListBlock ) => {
  return ( props ) => {

        const hasIcon = (
          typeof props.attributes.selectedIcon === "undefined" ||
          'None' === props.attributes.selectedIcon ||
          ''     === props.attributes.selectedIcon
        )? false : true;

        if( props.attributes && allowedBlocks.includes( props.name ) && hasIcon === true ) {

          return (
              <BlockListBlock
                  { ...props }
                  className={ 'has-icon icon-' + getIconSlug( props.attributes.selectedIcon ) }
              />
          );
        } else {
          return (
            <BlockListBlock
                { ...props }
            />
          )
        }
    };

}, 'withClientIdClassName' );

addFilter(
  'editor.BlockListBlock',
  'bipc/custom-classname',
  withClassName
);

//add filters
addFilter(
  'blocks.registerBlockType',
  'bipc/custom-attributes',
  addAttributes
);

addFilter(
  'editor.BlockEdit',
  'bipc/custom-advanced-control',
  withAdvancedControls
);

addFilter(
  'blocks.getSaveContent.extraProps',
  'bipc/applyExtraClass',
  applyExtraClass
);

wp.domReady( () => {

  wp.blocks.registerBlockStyle( 'core/button', {
      name: 'simple',
      label: 'Simple',
  } );

  wp.blocks.registerBlockStyle( 'core/cover', {
      name: 'header',
      label: 'Header',
  } );

} );