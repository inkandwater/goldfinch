const { __ }                = wp.i18n;
const { Fragment }          = wp.element;
const { InspectorControls, RichText, BlockControls, BlockIcon, MediaPlaceholder, MediaUploadCheck, MediaUpload, useBlockProps } = wp.blockEditor;
const { ToolbarGroup, Button, ToggleControl, PanelBody, PanelRow } = wp.components;

const edit = (( { attributes, setAttributes, className } ) => {

    const { files, fileIDs, showFileSize } = attributes;
    let hasFiles = false;

    const blockProps = useBlockProps();

    if( typeof files !== 'undefined' ) {
        hasFiles = files.length;
    }

    const onSelectFiles = ( files ) => {

        const IDs = files.map( file => {
            return file.id;
        } );

        setAttributes( {
            files: files,
            fileIDs: IDs
        } );

    }

    // Change file title from default.
    const onUpdateTitle = ( value, index ) => {

      // Add in a new value (customTitle) to a file object.
      setAttributes( {
        files: [
          ...files.slice( 0, index ),
          { ...files[ index ], customTitle: value },
          ...files.slice( index + 1 ),
        ]
      } );

    }

    const mediaPlaceholder = (
        <MediaPlaceholder
            className={ className }
            icon={ <BlockIcon /> }
            labels={ {
                title: __( 'File' ),
                instructions: __( 'Upload a file or pick one from your media library.' ),
            } }
            onSelect={ onSelectFiles }
            accept="*"
            multiple
            value={ hasFiles ? files : undefined }
        />
    );

    const fileControls = (
        <BlockControls>
            <MediaUploadCheck>
            <ToolbarGroup>
                <MediaUpload
                onSelect = { onSelectFiles }
                value={ fileIDs }
                multiple
                accept="*"
                render={ ( { open } ) => (
                    <Button
                    className = "components-toolbar__control"
                    label     = { __( 'Edit Files' ) }
                    icon      = { 'edit' }
                    onClick   = { open }
                    />
                ) }
                />
            </ToolbarGroup>
            </MediaUploadCheck>
        </BlockControls>
    );

    const toggleFileSize = (
        <InspectorControls>
            <PanelBody title={ __( 'Block Settings' ) }>
            <PanelRow>
                <ToggleControl
                label={ __( 'Show Filesize' ) }
                checked={ showFileSize }
                onChange={ () => setAttributes( {
                    showFileSize: ! showFileSize,
                } ) }
                />
            </PanelRow>
            </PanelBody>
        </InspectorControls>
    )

    // If there are no images, display the MediaPlaceholder
    if ( typeof files !== 'undefined' ) {

        // Get data for each file and output a table row.
        const renderTableRows = files.map( ( file, index ) => {
            let title = file.title;

            if( typeof file.customTitle !== 'undefined' ) {
                title = file.customTitle;
            }

            return (
                <tr class="file">
                    <td class="file__title">
                        <RichText
                            tagName="span" // must be block-level or else cursor disappears
                            value={ title }
                            placeholder={ __( 'Write file name…' ) }
                            onChange={ ( text ) => onUpdateTitle( text, index ) }
                        />
                    </td>

                    { !! showFileSize &&
                        <td class="file__meta">
                            <span class="file__size">{ file.filesizeHumanReadable }</span>
                        </td>
                    }
                </tr>
            )
        } );

        // Render when there are files selected
        return (
            <div { ... blockProps }>
                { fileControls }
                { toggleFileSize }
                <table>
                    <tbody>
                    { renderTableRows }
                    </tbody>
                </table>
            </div>
        );
    }

    // Render when no files selected
    return (
      <div { ... blockProps }>
        { mediaPlaceholder }
      </div>
    );

});

export default edit
