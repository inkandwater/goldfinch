const { useBlockProps } = wp.blockEditor;

const save = (( { attributes, className } ) => {

    const { files, showFileSize } = attributes;
    const blockProps = useBlockProps.save();

    if ( typeof files !== 'undefined' ) {

      // Get data for each file and output a table row.
      const renderFiles = files.map( file => {

        let title = file.title;
        let format;

        if ( typeof file.customTitle !== 'undefined' ) {
          title = file.customTitle;
        }

        switch(file.subtype) {
          case 'vnd.openxmlformats-officedocument.presentationml.presentation':
            format = 'powerpoint';
            break;
          case 'pdf':
            format = 'pdf';
            break;
          case 'jpeg':
            format = 'image';
            break;
          case 'png':
            format = 'image';
            break;
          case 'mpeg':
            format = 'video';
            break;
          case 'mp4':
            format = 'video';
            break;
          case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
            format = 'document';
            break;
          default:
            format = 'document';
        }

        return (
          <tr class="file">
            <td>
              <svg viewBox="0 0 88 98">
                  <use class="icon icon--format" href={ "#" + format + "-icon" }></use>
              </svg>
            </td>
            <td>
              <a class="file__title" href={ file.url } data-download={title} download>{ title }</a>
              { !! showFileSize &&
                <span class="file__meta"><span class="file__size">{ file.filesizeHumanReadable }</span></span>
              }
            </td>
          </tr>
        )

      } );

      return (
        <table { ... blockProps }>
          { renderFiles }
        </table>
      )

    }

});

export default save
