const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { InspectorControls, useBlockProps, withColors, PanelColorSettings, ContrastChecker, BlockControls, AlignmentToolbar } = wp.blockEditor;
const { PanelBody, RangeControl } = wp.components;

import FeatherIcon from 'feather-icons-react';
import Select from 'react-select';
import classnames from 'classnames';
import iconList from '../icon-list';
import { getIconName } from '../../../components/icon-name'

const edit = (({ attributes, setAttributes, className, clientId, setIconColor, iconColor, setBackgroundColor, backgroundColor }) => {

    const { selectedIcon, selectedOption, blockId, size, alignment } = attributes;
    let blockClass = getBlockDefaultClassName('goldfinch/icon');

    const blockProps = useBlockProps({
        className: className
    });

    React.useEffect(() => {
        if (!blockId) {
            setAttributes({ blockId: clientId });
        }
    }, []);

    let base = 8;
    let iconSize = base * size;

    const alignmentControls = (
        <InspectorControls>
            <BlockControls>
                <AlignmentToolbar
                    value={alignment}
                    onChange={(nextAlign) => {
                        setAttributes({ alignment: nextAlign });
                    }}
                />
            </BlockControls>
        </InspectorControls>
    );

    const iconOptions = iconList.map(icon => {
        return ({ label: getIconName(icon), value: icon })
    });

    const iconControls = (
        <InspectorControls>
            <PanelBody title="Icon" initialOpen={true}>
                <Select
                    label={__('Icon')}
                    className="select-wide"
                    value={selectedOption}
                    options={iconOptions}
                    onChange={option => {
                        setAttributes({ selectedIcon: option.value })
                        setAttributes({ selectedOption: option })
                    }}
                    placeholder={__('Choose Icon')}
                />
                <br></br>
                <RangeControl
                    label={__("Icon Size")}
                    value={size}
                    onChange={(value) => setAttributes({ size: value })}
                    min={1}
                    max={10}
                />
            </PanelBody>
            <PanelColorSettings
                title={__('Colour Settings')}
                colorSettings={[
                    {
                        label: __('Background Colour'),
                        value: backgroundColor.color,
                        onChange: setBackgroundColor,
                    },
                    {
                        label: __('Icon Colour'),
                        value: iconColor.color,
                        onChange: setIconColor,
                    }
                ]} />
            <PanelBody>
                <ContrastChecker
                    backgroundColor={backgroundColor.color}
                    fontSize={iconSize}
                    textColor={iconColor.color}
                />
            </PanelBody>
        </InspectorControls>
    );

    return (
        <div {...blockProps} data-block={blockId} data-align={alignment} data-icon={blockClass + "__" + selectedIcon}>

            {iconControls}
            {alignmentControls}

            {selectedIcon &&
                <div className={classnames(blockClass + "__icon-background", backgroundColor.class, { "has-background": backgroundColor.class !== undefined })}>
                    <FeatherIcon icon={selectedIcon} size={iconSize} stroke={iconColor.color} />
                </div>
            }

        </div>
    );


});

export default withColors(
    { iconColor: 'color', backgroundColor: 'background-color', }
)(edit)
