const { getBlockDefaultClassName } = wp.blocks;
const { useBlockProps, getColorClassName } = wp.blockEditor;

import FeatherIcon from 'feather-icons-react';
import classnames from 'classnames';

const save = ({ attributes, className }) => {

    const { selectedIcon, blockId, backgroundColor, iconColor, size, alignment } = attributes;
    let blockClass = getBlockDefaultClassName('goldfinch/icon');
    let backgroundColorClass;

    if (typeof backgroundColor !== undefined) {
        backgroundColorClass = getColorClassName('background-color', backgroundColor);
    }

    const blockProps = useBlockProps.save();

    let base = 8;
    let iconSize = base * size;
    let backgroundSize = iconSize + 16;

    let iconStyles = (selectedIcon) ? {} : { height: backgroundSize + "px", width: backgroundSize + "px" };

    return (
        <div {...blockProps} data-block={blockId} data-align={alignment} data-icon={blockClass + "__" + selectedIcon} style={iconStyles}>

            {selectedIcon &&
                <div className={classnames(blockClass + "__icon-background", backgroundColorClass, { "has-background": backgroundColorClass !== undefined })}>
                    <FeatherIcon role="img" focusable="false" aria-hidden="true" icon={selectedIcon} size={iconSize} stroke={"var( --wp--preset--color--" + iconColor + ')'} />
                </div>
            }

        </div>
    );

}

export default save
