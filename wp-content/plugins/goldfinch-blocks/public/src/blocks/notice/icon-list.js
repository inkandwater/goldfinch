const iconList = {
    alertCircle: {
        name: 'Notice',
        slug: 'alert-circle'
    },
    alertTriangle: {
        name: 'Warning',
        slug: 'alert-triangle'
    },
    checkCircle: {
        name: 'Success',
        slug: 'check-circle'
    },
    xOctagon: {
        name: 'Error',
        slug: 'x-octagon'
    }
}

export default iconList;

export const getIconName = ( icon ) => {
	return iconList[ icon ].name;
};

export const getIconSlug = ( icon ) => {
	return iconList[ icon ].slug;
};

export const getIconSvg = ( icon ) => {
	return iconList[ icon ].svg;
};