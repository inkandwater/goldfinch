import { getIconName, getIconSlug, getIconSvg } from './icon-list';
import classnames from 'classnames';

export const Icon = (props) => (

    <div className={ classnames( props.classes + "__icon", props.background ) }>
        { getIconSvg( props.icon ) }
        <style>{ `[data-block="${ props.id }"] svg .accent--primary { stroke: ${ props.primary } };` }</style>
        <style>{ `[data-block="${ props.id }"] svg .accent--secondary { stroke: ${ props.secondary }; }` }</style>
    </div>

);