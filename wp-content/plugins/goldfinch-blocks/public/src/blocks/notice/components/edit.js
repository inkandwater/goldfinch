const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { RichText, InspectorControls, useBlockProps } = wp.blockEditor;
const { PanelBody } = wp.components;

import FeatherIcon from 'feather-icons-react';
import Select from 'react-select';

import iconList, { getIconSlug, getIconName } from '../icon-list';

const edit = ( { attributes, className, setAttributes, clientId } ) => {

    const { blockId, content, align, selectedAlert, selectedOption } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/notice' );
    const blockProps = useBlockProps();

    React.useEffect( () => {
        if ( !blockId ) {
            setAttributes( { blockId: clientId } );
        }
    }, [] );

    const iconOptions = Object.keys( iconList ).map( icon => {
        return ({ label: getIconName(icon), value: getIconSlug(icon) })
    });

    const iconControls = (
        <InspectorControls>
            <PanelBody title="Icon" initialOpen={ true }>
                <Select
                    label    = { __( 'Icon' ) }
                    className="select-wide"
                    value    = { selectedOption }
                    options  = { iconOptions }
                    onChange = { option => {
                        setAttributes( { selectedAlert: option.value } )
                        setAttributes( { selectedOption: option } )
                    }}
                    placeholder = { __( 'Choose Icon' ) }
                />
            </PanelBody>
        </InspectorControls>
    );

    return (
        <div { ...blockProps } data-block={ blockId } data-alert={ selectedOption.label }>

            { iconControls }

            <figure className={ blockClass + "__icon" }>
                <FeatherIcon icon={selectedAlert} />
            </figure>

            <div className={ blockClass + "__body" }>

                <RichText
                    tagName={ "span" }
                    value={ content }
                    allowedFormats={ false }
                    onChange={ ( content ) => setAttributes( { content } ) }
                    placeholder={ __( 'Notice text...' ) }
                />

            </div>

        </div>
    );

}

export default edit