const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { RichText, useBlockProps } = wp.blockEditor;

import FeatherIcon from 'feather-icons-react';

const save = ( { attributes, className, setAttributes, clientId } ) => {

    const { blockId, content, align, selectedAlert, selectedOption } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/notice' );
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps } data-block={ blockId } data-alert={ selectedOption.label }>

            <figure className={ blockClass + "__icon" }>
                <FeatherIcon icon={selectedAlert} />
            </figure>

            <div className={ blockClass + "__body" }>

                <RichText.Content
                    tagName={ "span" }
                    value={ content }
                />

            </div>

        </div>
    );

}

export default save