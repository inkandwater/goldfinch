( function( $ ) {
    'use strict';

    $( window ).load( function(){

        // Initiate a map for each instance of the map canvas element.
        $( '.map-canvas' ).each( function(){
            initializeMap( $(this) );
        });

    });

    function initializeMap( mapInstance ) {

        // Set some variables
        var mapCanvas,
            latitude,
            longitude,
            zoom,
            mapOptions,
            map,
            customIcon,
            iconColor,
            markerOptions,
            marker,
            mapCenter,
            mapstyle;


        //Get the element we're going to use for the map canvas
        mapCanvas = mapInstance;

        // User Defined
        zoom      = mapInstance.data( 'zoom' );
        iconColor = '#e6634c';
        latitude  = mapInstance.data( 'lat' );
        longitude = mapInstance.data( 'long' );

        var LatLng = new google.maps.LatLng( latitude, longitude );

        // Set the zoom level
        zoom = parseInt( zoom );

        mapstyle = [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#6195a0"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#e6f3d6"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f0e8e0"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#f0e8e0"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f4f4f4"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#787878"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#eaf6f8"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#eaf6f8"
                    }
                ]
            }
        ];

        // Map Options
        mapOptions = {
            center: LatLng,
            draggable: false,
            zoom: zoom,
            disableDefaultUI: true,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            styles: mapstyle,
            backgroundColor: '#F1F1F1'
        };

        // Fire up the map.
        map = new google.maps.Map( mapCanvas[0], mapOptions );

        customIcon = {
            path: 'M15.9722106,12.2916595 C15.9722106,14.2040921 14.4124252,15.7638775 12.4999925,15.7638775 C10.5875599,15.7638775 9.02777447,14.2040921 9.02777447,12.2916595 C9.02777447,10.3792268 10.5875599,8.81944138 12.4999925,8.81944138 C14.4124252,8.81944138 15.9722106,10.3792268 15.9722106,12.2916595 L15.9722106,12.2916595 Z M25,12.5 C25,5.59082031 19.4091797,0 12.5,0 C5.59082031,0 0,5.59082031 0,12.5 C0,13.9892578 0.170898438,15.5273438 0.805664062,16.8701172 L9.71679688,35.7666016 C10.2050781,36.8408203 11.328125,37.5 12.5,37.5 C13.671875,37.5 14.7949219,36.8408203 15.3076172,35.7666016 L24.1943359,16.8701172 C24.8291016,15.5273438 25,13.9892578 25,12.5 L25,12.5 Z',
            fillColor: iconColor,
            fillOpacity: 1,
            strokeWeight: 0,
            scale: 1,
            anchor: new google.maps.Point(12, 38)
        };

        // Marker
        markerOptions = {
            map: map,
            icon: customIcon
        };
        marker = new google.maps.Marker( markerOptions );
        marker.setPosition( LatLng );

        //Now that we're ready, add a class to style the canvas
        mapCanvas.addClass( 'active' );

        // Resize map when meta box is opened
        $( window ).resize( function() {

            mapCenter = map.getCenter();
            google.maps.event.trigger( map, 'resize' );
            map.setCenter( mapCenter );

        });

    }

})( jQuery );