const { useBlockProps } = wp.blockEditor;

const save = ( ({ attributes, className }) => {

    const { zoom, latitude, longitude, address } = attributes;
    const blockProps = useBlockProps.save();

    return (
        <div { ...blockProps }>
            <div class="map-canvas" data-lat={ latitude } data-long={ longitude } data-zoom={ zoom } data-address={ address }></div>
        </div>
    );

});

export default save