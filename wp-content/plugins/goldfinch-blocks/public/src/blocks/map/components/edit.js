const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { Fragment } = wp.element;
const { InspectorControls, useBlockProps } = wp.blockEditor;
const { PanelBody, RangeControl, TextControl, Button } = wp.components;

import Geocode from "react-geocode";

const edit = ( ({ attributes, setAttributes, className }) => {

        const { align, zoom, latitude, longitude, address } = attributes;
        const blockClass = getBlockDefaultClassName( 'goldfinch/map' );
        const blockProps = useBlockProps();

        let paramsObject = {
            size: '640x500',
            scale: '2',
            zoom: zoom,
            center: latitude + ',' + longitude,
            key: blockData.apiKey,
            markers: 'color:#E31959|size:small|' + latitude + ',' + longitude,
        }

        function buildURL( obj ) {
            let str = [];
            for ( let p in obj )
                if ( obj.hasOwnProperty(p) ) {
                    str.push( encodeURIComponent(p) + "=" + encodeURIComponent( obj[p] ) );
                }
            return str.join( "&" );
        }

        let params    = buildURL( paramsObject );
        let staticURL = 'https://maps.googleapis.com/maps/api/staticmap?' + params;

        function geolocation( location ) {

            if ( location.length >= 5 ) {

                Geocode.setApiKey( "AIzaSyAE_2aXAa46Vc-ziYcOiWtJl3qcSD2IkjU" );
                Geocode.fromAddress(location).then(

                    response => {
                        const { lat, lng } = response.results[0].geometry.location;

                        setAttributes( { latitude: lat }  );
                        setAttributes( { longitude: lng } );

                    },
                    error => {
                        console.error(error);
                    }
                );

            }
        }

        const addressLookup = (
            <div className={ blockClass + "__address-lookup" }>
                <h3 className={ blockClass + "__lookup-label" }>{ __( 'Address' ) }</h3>

                <TextControl
                    key="query-controls-type-text"
                    label={ __( 'Enter Address' ) }
                    description={ "The street address that you want to look up, in the format used by your postal service." }
                    onChange={ ( value ) => { setAttributes( { address: value } ) } }
                    value={ address }
                />

                <Button
                    isPrimary
                    onClick={ geolocation( address ) }
                    text={ __( 'Find Coordinates' ) }>
                </Button>

            </div>
        );

        const zoomControls = (
            <InspectorControls>
                <PanelBody>
                    <RangeControl
                        beforeIcon = "search"
                        label      = { 'Zoom' }
                        value      = { zoom }
                        onChange   = { ( zoom ) => {
                            setAttributes( { zoom } );
                        } }
                        min = { 1 }
                        max = { 21 }
                    />
                    { addressLookup }
                </PanelBody>
            </InspectorControls>
        );

        return (
            <div { ...blockProps }>
                { ! latitude && ! longitude &&
                    addressLookup
                }
                { !! address && !! latitude && !! longitude &&
                    <Fragment>
                        { zoomControls }
                        <img style={{ width: '100%' }} src={ staticURL }/>
                    </Fragment>
                }
            </div>
    ) });

    export default edit