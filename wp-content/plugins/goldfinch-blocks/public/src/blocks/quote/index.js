import './editor.scss';
import classNames from 'classnames';

const { __ } = wp.i18n;
const { addFilter } = wp.hooks;
const { Fragment }  = wp.element;
const { BlockAlignmentToolbar, BlockControls } = wp.blockEditor;
const { createHigherOrderComponent } = wp.compose;
const { ToolbarGroup } = wp.components;

//restrict to specific block names
const allowedBlocks = [ 'core/quote' ];

/**
 * Add custom attribute for mobile visibility.
 *
 * @param {Object} settings Settings for the block.
 *
 * @return {Object} settings Modified settings.
 */
 function addAttributes( settings ) {

    //check if object exists for old Gutenberg version compatibility
    //add allowedBlocks restriction
    if( typeof settings.attributes !== 'undefined' && allowedBlocks.includes( settings.name ) ){

      settings.attributes = Object.assign( settings.attributes, {
        alignment: {
          type: 'string',
          default: 'center'
        }
      });

    }

    return settings;
}


/**
 * Add mobile visibility controls on Advanced Block Panel.
 *
 * @param {function} BlockEdit Block edit component.
 *
 * @return {function} BlockEdit Modified block edit component.
 */
 const withAdvancedControls = createHigherOrderComponent( ( BlockEdit ) => {
    return ( props ) => {

      const {
        name,
        attributes,
        setAttributes,
        isSelected,
      } = props;

      const { alignment } = attributes;

      return (
        <Fragment>
          <BlockEdit {...props} />

          { isSelected && allowedBlocks.includes( name ) &&
            <BlockControls>
              <ToolbarGroup>
                <BlockAlignmentToolbar
                    value={ alignment }
                    onChange={ ( nextAlign ) => {
                        setAttributes( { alignment: nextAlign } );
                    } }
                />
              </ToolbarGroup>
            </BlockControls>
          }

        </Fragment>
      );
    };
}, 'withAdvancedControls');

const withClassName = createHigherOrderComponent( ( BlockListBlock ) => {
    return ( props ) => {

          if( props.attributes && allowedBlocks.includes( props.name ) ) {

            return (
                <BlockListBlock
                    { ...props }
                    className={ 'align' + props.attributes.alignment }
                />
            );
          } else {
            return (
              <BlockListBlock
                  { ...props }
              />
            )
          }
      };

  }, 'withClientIdClassName' );

addFilter(
    'editor.BlockListBlock',
    'goldfinch/custom-classname',
    withClassName
);

//add filters
addFilter(
    'blocks.registerBlockType',
    'goldfinch/custom-attributes',
    addAttributes
);

addFilter(
    'editor.BlockEdit',
    'goldfinch/custom-advanced-control',
    withAdvancedControls
);