<?php
/**
 * Renders the `goldfinch/faqs` block on server.
 *
 * @param array $attributes The block attributes.
 *
 * @return string Returns the post content with latest posts added.
 */
function render_faqs_block( $attributes ) {

    $block_content = '';

    ob_start();

    goldfinch_faqs( $attributes );

    $block_content = ob_get_contents();
    ob_end_clean();

    return $block_content;

}

/**
 * Registers the `goldfinch/faqs` block on server.
 */
function register_faqs_block() {

    register_block_type(
        'goldfinch/faqs',
        array(
            'attributes'      => array(
                'align' => array(
                    'type' => 'string',
                    'default' => 'wide'
                ),
                'childBlocks' => array(
                    'type' => 'array'
                ),
                'blockClasses' => array(
                    'type' => 'string'
                ),
            ),
            'render_callback' => 'render_faqs_block',
        )
    );

}

add_action( 'init', 'register_faqs_block' );