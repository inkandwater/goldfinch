const { getBlockDefaultClassName } = wp.blocks;
const { RichText, InnerBlocks, useBlockProps } = wp.blockEditor;

const save = ( { attributes, className } ) => {

    const { title, snippet, alignment, isStacked } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/icon-text' );
    const blockProps = useBlockProps.save();

    const variation = (isStacked) ? 'stacked': 'inline';

    return (
        <div { ...blockProps } data-variation={ variation } data-alignment={ alignment }>

            <InnerBlocks.Content template = { [['goldfinch/icon']] } />

            <div className={ blockClass + "__body" }>

                { !! title &&
                    <RichText.Content
                        className={ blockClass + '__title' }
                        tagName="p"
                        value={ title }
                    />
                }

                <RichText.Content
                    className={ blockClass + '__snippet' }
                    tagName="p"
                    value={ snippet }
                />

            </div>

        </div>
    );

}

export default save