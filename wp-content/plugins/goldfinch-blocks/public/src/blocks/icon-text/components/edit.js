const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { RichText, InnerBlocks, AlignmentToolbar, BlockControls, InspectorControls, useBlockProps } = wp.blockEditor;

const ALLOWED_BLOCKS = [ 'goldfinch/icon' ];

const edit = ( { attributes, className, setAttributes } ) => {

    // Setup the attributes
    const { title, snippet, alignment, isStacked } = attributes;
    const blockClass = getBlockDefaultClassName( 'goldfinch/icon-text' );
    const blockProps = useBlockProps();

    const alignmentControls = (
        <InspectorControls>
            <BlockControls>
                <AlignmentToolbar
                    value={ alignment }
                    onChange={ ( nextAlign ) => {
                        setAttributes( { alignment: nextAlign } );
                    } }
                />
            </BlockControls>
        </InspectorControls>
    );

    const variation = (isStacked) ? 'stacked': 'inline';

    return (
        <div { ...blockProps } data-variation={ variation } data-alignment={ alignment }>

            { !! isStacked && alignmentControls }

            <InnerBlocks
                template      = { [['goldfinch/icon']] }
                templateLock  = { 'all' }
                allowedBlocks = { ALLOWED_BLOCKS }
            />

            <div className={ blockClass + "__body" }>

                <RichText
                    className={ blockClass + '__title' }
                    tagName="p"
                    value={ title }
                    onChange={ ( title ) => setAttributes( { title } ) }
                    placeholder={ __( 'Add a title...(Optional)' ) }
                />

                <RichText
                    className={ blockClass + '__snippet' }
                    tagName="p"
                    value={ snippet }
                    onChange={ ( snippet ) => setAttributes( { snippet } ) }
                    placeholder={ __( 'Add a snippet...' ) }
                />

            </div>

        </div>
    );

}

export default edit