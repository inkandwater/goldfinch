const { registerPlugin } = wp.plugins;

import SidebarComponent from './components/sidebar';

registerPlugin( "gf-sidebar-page-header", {
    icon: 'admin-settings',
    render: SidebarComponent
});