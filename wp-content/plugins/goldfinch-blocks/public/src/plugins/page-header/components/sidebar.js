const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { PluginSidebarMoreMenuItem, PluginSidebar } = wp.editPost;
const { PanelBody, PanelRow } = wp.components;

import SidebarPostMeta from './meta';

const SidebarComponent = () => {
    return(
        <Fragment>
            <PluginSidebar
                name="gf-sidebar-page-header"
                title={ __( 'Page Header Settings', 'goldfinch' ) }>

                <PanelBody initialOpen={true}>
                    <PanelRow>
                        <SidebarPostMeta />
                    </PanelRow>
                </PanelBody>

            </PluginSidebar>
            <PluginSidebarMoreMenuItem target='gf-sidebar-page-header'>
                { __('PE Settings', 'goldfinch' ) }
            </PluginSidebarMoreMenuItem>
        </Fragment>
    );
}

export default SidebarComponent;