const { __ } = wp.i18n;
const { withDispatch, withSelect } = wp.data;
const { compose } = wp.compose;
const { ToggleControl } = wp.components;

const EnableSidebarMetaComponent = (props) => {
    return(
        <ToggleControl
            label    = { __('Enable Light Header', 'goldfinch') }
            checked  = { props.enable }
            onChange = { props.setPostMeta }
        />
    );
}

const SidebarPostMeta = compose([

    withSelect(select => {
        return { enable: select('core/editor').getEditedPostAttribute('meta')['_gf_header_theme_light'] }
    }),
    withDispatch(dispatch => {
        return {
            setPostMeta: function(value) {
                dispatch('core/editor').editPost({ meta: { _gf_header_theme_light: value } });
            }
        }
    })

])(EnableSidebarMetaComponent);

export default SidebarPostMeta;