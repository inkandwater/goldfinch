export const getIconName = (icon) => {

    const nameWithSpaces = icon.replace(/-/g, ' ');
    const nameAsArray = nameWithSpaces.split(" ");

    for (var i = 0; i < nameAsArray.length; i++) {
        nameAsArray[i] = nameAsArray[i].charAt(0).toUpperCase() + nameAsArray[i].slice(1);
    }

    const name = nameAsArray.join(" ");

    return name;

};
