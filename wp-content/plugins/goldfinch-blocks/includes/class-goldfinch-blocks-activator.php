<?php

/**
 * Fired during plugin activation
 *
 * @link       inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Goldfinch_Blocks_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
