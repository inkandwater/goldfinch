<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Goldfinch_Blocks_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'goldfinch-blocks',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
