<?php

/**
 * Fired during plugin deactivation
 *
 * @link       inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Blocks
 * @subpackage Goldfinch_Blocks/includes
 * @author     Paul Myers <paul@inkandwater.co.uk>
 */
class Goldfinch_Blocks_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
