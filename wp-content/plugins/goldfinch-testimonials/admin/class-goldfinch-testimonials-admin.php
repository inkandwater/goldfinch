<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/admin
 * @author     Ink & Water Ltd <support@inkandwater.co.uk>
 */
class Goldfinch_Testimonials_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style(
            'goldfinch_blocks_editor_styles',
            plugins_url( 'public/build/build.editor.css', dirname( __FILE__ ) ),
            array( 'wp-edit-blocks' ),
            $this->version
        );

    }

    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Goldfinch_Blocks_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Goldfinch_Blocks_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script(
            'goldfinch_blocks',
            plugins_url( 'public/build/index.js', dirname( __FILE__ ) ),
            array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-block-editor' ),
            $this->version,
            true
        );

    }

    public function blocks_post_meta() {

        register_post_meta( 'testimonial', '_goldfinch_testimonial_star_rating', array(
            'show_in_rest'      => true,
            'single'            => true,
            'type'              => 'number',
            'auth_callback'     => function() {
                return current_user_can( 'edit_posts' );
            }
        ) );

    }

    public function add_testimonial_cpt() {

        $labels = array(
            'name'                  => _x( 'Testimonials', 'Post Type General Name', $this->plugin_name ),
            'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', $this->plugin_name ),
            'menu_name'             => __( 'Testimonials', $this->plugin_name ),
            'name_admin_bar'        => __( 'Testimonial', $this->plugin_name ),
            'archives'              => __( 'Testimonial Archives', $this->plugin_name ),
            'attributes'            => __( 'Testimonial Attributes', $this->plugin_name ),
            'parent_item_colon'     => __( 'Parent Testimonial:', $this->plugin_name ),
            'all_items'             => __( 'All Testimonials', $this->plugin_name ),
            'add_new_item'          => __( 'Add New Testimonial', $this->plugin_name ),
            'add_new'               => __( 'Add New', $this->plugin_name ),
            'new_item'              => __( 'New Testimonial', $this->plugin_name ),
            'edit_item'             => __( 'Edit Testimonial', $this->plugin_name ),
            'update_item'           => __( 'Update Testimonial', $this->plugin_name ),
            'view_item'             => __( 'View Testimonial', $this->plugin_name ),
            'view_items'            => __( 'View Testimonials', $this->plugin_name ),
            'search_items'          => __( 'Search Testimonial', $this->plugin_name ),
            'not_found'             => __( 'Not found', $this->plugin_name ),
            'not_found_in_trash'    => __( 'Not found in Trash', $this->plugin_name ),
            'featured_image'        => __( 'Featured Image', $this->plugin_name ),
            'set_featured_image'    => __( 'Set featured image', $this->plugin_name ),
            'remove_featured_image' => __( 'Remove featured image', $this->plugin_name ),
            'use_featured_image'    => __( 'Use as featured image', $this->plugin_name ),
            'insert_into_item'      => __( 'Insert into item', $this->plugin_name ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', $this->plugin_name ),
            'items_list'            => __( 'Items list', $this->plugin_name ),
            'items_list_navigation' => __( 'Items list navigation', $this->plugin_name ),
            'filter_items_list'     => __( 'Filter items list', $this->plugin_name ),
        );

        $args = array(
            'label'                 => __( 'Testimonial', $this->plugin_name ),
            'description'           => __( 'Reviews from Belvedere Rosa', $this->plugin_name ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'custom-fields' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 15,
            'menu_icon'             => 'dashicons-format-status',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => true,
            'rewrite'               => false,
            'capability_type'       => 'post',
            'show_in_rest'          => true,
            'rest_base'             => 'testimonials',
            'template_lock'         => 'all',
            'template' => array(
                array( 'goldfinch/star-rating', array() ),
                array( 'core/paragraph', array() ),
                array( 'core/heading', array( 'placeholder' => 'Add Author...', 'level' => 4 ) ),
            )
        );

        register_post_type( 'testimonial', $args );
    }
}