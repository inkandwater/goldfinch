<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/includes
 * @author     Ink & Water Ltd <support@inkandwater.co.uk>
 */
class Goldfinch_Testimonials_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
