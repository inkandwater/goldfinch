<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Goldfinch_Testimonials
 * @subpackage Goldfinch_Testimonials/includes
 * @author     Ink & Water Ltd <support@inkandwater.co.uk>
 */
class Goldfinch_Testimonials_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
