<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://inkandwater.co.uk
 * @since             1.0.0
 * @package           Goldfinch_Testimonials
 *
 * @wordpress-plugin
 * Plugin Name:       GOLDFINCH Testimonials
 * Plugin URI:        https://inkandwater.co.uk
 * Description:       Adds Testimonial post type for the Goldfinch theme.
 * Version:           1.0.0
 * Author:            Ink & Water
 * Author URI:        https://inkandwater.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       goldfinch-testimonials
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GOLDFINCH_TESTIMONIALS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-goldfinch-testimonials-activator.php
 */
function activate_goldfinch_testimonials() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-goldfinch-testimonials-activator.php';
	Goldfinch_Testimonials_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-goldfinch-testimonials-deactivator.php
 */
function deactivate_goldfinch_testimonials() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-goldfinch-testimonials-deactivator.php';
	Goldfinch_Testimonials_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_goldfinch_testimonials' );
register_deactivation_hook( __FILE__, 'deactivate_goldfinch_testimonials' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-goldfinch-testimonials.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_goldfinch_testimonials() {

	$plugin = new Goldfinch_Testimonials();
	$plugin->run();

}

run_goldfinch_testimonials();