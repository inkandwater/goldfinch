export const Star = (props) => (

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">

        <defs>
            <mask id={`star-rating-${props.id}-${props.index}`}>
                <rect height="24" width={Math.max( 0, Math.min(props.value, 1) ) * 24} y="0" x="0" fill="#fff" />
            </mask>
        </defs>

        <path className="star-outline" fill={"none"} strokeWidth="1" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
        <path className="star" mask={`url(#star-rating-${props.id}-${props.index})`} strokeWidth="1" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />

    </svg>

);