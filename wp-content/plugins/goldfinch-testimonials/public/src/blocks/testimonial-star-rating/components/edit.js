const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { InspectorControls, useBlockProps } = wp.blockEditor;
const { PanelBody, RangeControl } = wp.components;

import { Star } from "../../../components/star";

const edit = (( { clientId, attributes, setAttributes, className } ) => {

    const { starCount, selectedStars, rating } = attributes;
    const blockClass = getBlockDefaultClassName( "goldfinch/star-rating" );
    const blockProps = useBlockProps();

    const onChangeRating = ( value ) => {

        setAttributes( { selectedStars: value } );
        setAttributes( { rating: value } );

    };

    const blockControls = (
        <InspectorControls>
            <PanelBody title={ __('Star Settings' )}>
                <RangeControl
                    label={ __("Star value") }
                    value={ selectedStars }
                    onChange={ onChangeRating }
                    min={ 0 }
                    max={ starCount }
                    step={ 0.5 }
                />
            </PanelBody>
        </InspectorControls>
    );

    return (
        <div { ... blockProps }>

            {blockControls}

            { [...Array(starCount) ].map((e, i) => (
                <div className={ blockClass + "__star" }>
                    <Star
                        id={clientId}
                        index={i}
                        value={ selectedStars - i }
                    />
                </div>
            ))}

        </div>
    );

});

export default edit
