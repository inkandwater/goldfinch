const { __ } = wp.i18n;
const { getBlockDefaultClassName } = wp.blocks;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, RangeControl } = wp.components;
const { withSelect } = wp.data;
const { RawHTML } = wp.element;

import pickBy from 'lodash/pickBy';
import isUndefined from 'lodash/isUndefined';

import { Star } from "../../../components/star";

const edit = ( ({ testimonials, className, attributes: { align, postsToShow, starCount }, setAttributes }) => {

    if ( ! testimonials ) {
        return "No Testimonials Found";
    }

    const blockClass = getBlockDefaultClassName( 'goldfinch/testimonials' );

    const onChangePerPage = ( value ) => {
        setAttributes( { postsToShow: value } );
    };

    const blockControls = (
        <InspectorControls>
            <PanelBody title={__('Sorting')}>
                <RangeControl
                    label={ __("Number of items") }
                    value={ postsToShow }
                    onChange={ onChangePerPage }
                    min={ 4 }
                    max={ 12 }
                    step={ 1 }
                />
            </PanelBody>
        </InspectorControls>
    );

    const testimonialsToShow = testimonials.map( (testimonial, i) => {

        return (
            <article key={i} className={ blockClass + "__testimonial"}>

                <h3 className={ blockClass + "__title" }><RawHTML>{testimonial.title.rendered}</RawHTML></h3>
                <div className={ blockClass + "__star-rating" }>
                    { [...Array(5) ].map((e, i) => (
                        <div className={ blockClass + "__star" }>
                            <Star id={testimonial.id} index={i} value={ (testimonial.meta._goldfinch_testimonial_star_rating - i) } />
                        </div>
                    ))}
                </div>
                <p className={ blockClass + "__description" }><RawHTML>{testimonial.content.rendered}</RawHTML></p>

            </article>
        );

    });

    return (
        <div className={className}>

            { blockControls }
            { testimonialsToShow }

        </div>
    );

});

export default withSelect( ( select, props ) => {

    // Get Data.
    const { postsToShow } = props.attributes;
    const { getEntityRecords } = select( 'core' );

    const orderQuery = pickBy(
        {
            order: 'asc',
            orderby: 'date',
            per_page: postsToShow
        },
        ( value ) => ! isUndefined( value )
    );

    return {
        testimonials: getEntityRecords( 'postType', 'testimonial', orderQuery )
    };

}) ( edit )
