const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import "./editor.scss";
import metadata from './block.json';
import edit from './components/edit';

registerBlockType( metadata, {
    title: metadata.title,
    category: "custom-goldfinch-category",
    icon: <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#cf4830" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z" fill="#cf4830"></path></svg>,
    edit,
    save() {
        return null
    }
});