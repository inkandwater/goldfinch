<?php
/**
 * Renders the `goldfinch/testimonials` block on server.
 *
 * @param array $attributes The block attributes.
 *
 * @return string Returns the post content with latest posts added.
 */
function render_testimonials_block( $attributes ) {

    $block_content = '';

    ob_start();

    goldfinch_testimonials( $attributes );

    $block_content = ob_get_contents();
    ob_end_clean();

    return $block_content;

}

/**
 * Registers the `goldfinch/testimonials` block on server.
 */
function register_testimonials_block() {

    register_block_type(
        'goldfinch/testimonials',
        array(
            'attributes'      => array(
                'align' => array(
                    'type' => 'string',
                    'default' => 'full'
                ),
                'postsToShow' => array(
                    'type' => 'number',
                    'default' => 4
                ),
                'starCount' => array(
                    'type' => "number",
                    'default' => 5,
                ),
            ),
            'render_callback' => 'render_testimonials_block',
        )
    );

}

add_action( 'init', 'register_testimonials_block' );