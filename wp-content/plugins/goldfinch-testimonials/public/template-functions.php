<?php
if ( ! function_exists( 'goldfinch_testimonials' ) ) :

    function goldfinch_testimonials( $attributes ) {

        $class = array( 'wp-block-goldfinch-testimonials' );
        $block_class = 'wp-block-goldfinch-testimonial';
        $align = $attributes['align'] ?? null;

        if( $align ) {
            $class[] = "align{$align}";
        } else {
            $class[] = null;
        }

        if( isset( $attributes["className"] ) ) :
            $class[] = $attributes["className"];
        endif;

        $args = array(
            'post_status' => 'publish',
            'post_type'   => 'testimonial',
            'orderby'     => 'date title',
            'order'       => 'ASC'
        );

        if( $attributes['postsToShow'] ) {
            $args['posts_per_page'] = $attributes['postsToShow'];
        }

        $testimonials = new WP_Query( $args );

        $no_stars = $attributes['starCount'];
        $stars  = range(1, $no_stars);

        if ( !is_admin() && $testimonials->have_posts()  ) : ?>

            <div class="<?php echo implode( " ", $class ); ?>">

                <?php while( $testimonials->have_posts() ) : $testimonials->the_post();

                    $rating = goldfinch_testimonial_star_rating( get_the_ID() ); ?>

                    <article id="testimonial-<?php echo get_the_ID() ?>" class="wp-block-goldfinch-testimonial">

                        <div class="<?php echo $block_class; ?>__star-rating">

                            <span class="<?php echo $block_class . '__score screen-reader-text'; ?>"><?php printf( "Review score is %s out of 5", esc_attr( $rating ) ); ?></span>
                            <?php foreach( $stars as $index => $star ) :

                                $min = min( floatval( $rating) - $index, 1 );
                                $max = max( 0, $min );

                                $width = $max * 24; ?>

                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false">
                                    <defs><mask id="star-<?php echo get_the_ID() . '-' . $star; ?>"><rect height="24" width="<?php echo $width; ?>" y="0" x="0" fill="#fff" /></mask></defs>
                                    <path class="star-outline" fill="none" stroke-width="1" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
                                    <path class="star" mask="url(#star-<?php echo get_the_ID() . '-' . $star; ?>)" stroke-width="1" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
                                </svg>

                            <?php endforeach ?>
                        </div>

                        <?php printf( '<h3 class="%s__title">%s</h3>', esc_html( $block_class ), esc_html( get_the_title( get_the_ID() ) )); ?>
                        <?php printf( '<div class="%s__description">%s</div>', esc_html( $block_class ), wp_kses_post( get_the_content( null, false, get_the_ID() ) ) ); ?>

                    </article>

                <?php endwhile ?>

            </div>

        <?php endif;

    }

endif;

if ( ! function_exists( 'goldfinch_testimonial_star_rating' ) ) :

    function goldfinch_testimonial_star_rating( $id ) {

        $star_rating = get_post_meta( $id, '_goldfinch_testimonial_star_rating', true );

        return $star_rating;

    }

endif;