=== Testimonials ===
Contributors: (tomnapier, paulmyers)
Requires at least: 5.9.1
Tested up to: 5.9
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds Testimonial post type for the Goldfinch theme.

Changelog
---------

* 1.0.0 *
- Initial release.